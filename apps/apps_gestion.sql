-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : lun. 25 oct. 2021 à 13:28
-- Version du serveur :  5.7.32
-- Version de PHP : 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `apps_gestion`
--

-- --------------------------------------------------------

--
-- Structure de la table `APPLICATIONS`
--

CREATE TABLE `APPLICATIONS` (
  `id` int(11) NOT NULL,
  `nom_dossier` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `nom_bdd` varchar(255) DEFAULT NULL,
  `statut` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `APPLICATIONS`
--

INSERT INTO `APPLICATIONS` (`id`, `nom_dossier`, `nom`, `logo`, `nom_bdd`, `statut`) VALUES
(1, 'apps', 'MYDIGITAL', 'img/logo_navbar-mydigital.png', 'apps_gestion', '-1');

-- --------------------------------------------------------

--
-- Structure de la table `PROFILS`
--

CREATE TABLE `PROFILS` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `id_APPLICATIONS` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `PROFILS`
--

INSERT INTO `PROFILS` (`id`, `nom`, `id_APPLICATIONS`) VALUES
(1, 'Vision', 1),
(2, 'Utilisateur', 1),
(3, 'Superviseur', 1),
(4, 'Admin', 1);

-- --------------------------------------------------------

--
-- Structure de la table `ROLES`
--

CREATE TABLE `ROLES` (
  `id` int(11) NOT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `tag_local_exist` tinyint(1) DEFAULT NULL,
  `id_APPLICATIONS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ROLES`
--

INSERT INTO `ROLES` (`id`, `tag`, `descr`, `tag_local_exist`, `id_APPLICATIONS`) VALUES
(1, 'CONNECT', 'Connexion à MyDigital', 1, 1),
(2, 'USERS_VIEW', 'Accès à la page des utilisateurs', 1, 1),
(3, 'USERS_ADD', 'Ajout d\'un utilisateur', 1, 1),
(4, 'USERS_EDIT_STATUT', 'Modification du statut d\'un utilisateur', 1, 1),
(5, 'USERS_EDIT_ROLES', 'Modification des rôles d\'un utilisateur', 1, 1),
(6, 'USERS_DELETE', 'Suppression d\'un utilisateur', 1, 1),
(7, 'APPS_VIEW', 'Accès à la page des applications', 1, 1),
(8, 'APPS_EDIT_STATUT', 'Modification du statut d\'une application', 1, 1),
(9, 'APPS_ADD_PROFILS', 'Création d\'un profil sur une application', 1, 1),
(10, 'APPS_EDIT_PROFILS', 'Edition d\'un profil sur un application', 1, 1),
(11, 'APPS_DELETE_PROFILS', 'Suppresion d\'un profil sur une application', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ROLES_PROFILS`
--

CREATE TABLE `ROLES_PROFILS` (
  `id_PROFILS` int(11) NOT NULL,
  `id_ROLES` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ROLES_PROFILS`
--

INSERT INTO `ROLES_PROFILS` (`id_PROFILS`, `id_ROLES`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(3, 2),
(4, 2),
(4, 3),
(3, 4),
(4, 4),
(4, 5),
(4, 6),
(4, 7),
(4, 8),
(4, 9),
(4, 10),
(4, 11);

-- --------------------------------------------------------

--
-- Structure de la table `UTILISATEURS`
--

CREATE TABLE `UTILISATEURS` (
  `id` int(11) NOT NULL,
  `guid` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mdp` varchar(255) DEFAULT NULL,
  `statut` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `UTILISATEURS`
--

INSERT INTO `UTILISATEURS` (`id`, `guid`, `nom`, `prenom`, `email`, `mdp`, `statut`) VALUES
(1, 'C90605A1-799C-4CAF-9F09-C123293E8F8E', 'PICART', 'Mathieu', 'mathieu.picart@ca-corse.fr', '$2y$10$vvjy9AaG1qeqXUw6bDL8uum8c43IU10zmOWC3yfH31BN0YJgLczEe', '1');

-- --------------------------------------------------------

--
-- Structure de la table `UTILISATEURS_ROLES`
--

CREATE TABLE `UTILISATEURS_ROLES` (
  `id_ROLES` int(11) NOT NULL,
  `id_UTILISATEURS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `UTILISATEURS_ROLES`
--

INSERT INTO `UTILISATEURS_ROLES` (`id_ROLES`, `id_UTILISATEURS`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(19, 1),
(20, 1),
(21, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `APPLICATIONS`
--
ALTER TABLE `APPLICATIONS`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ROLES_APPLICATIONS_FK` (`id`);

--
-- Index pour la table `PROFILS`
--
ALTER TABLE `PROFILS`
  ADD PRIMARY KEY (`id`),
  ADD KEY `PROFILS_APPLICATIONS_FK` (`id_APPLICATIONS`);

--
-- Index pour la table `ROLES`
--
ALTER TABLE `ROLES`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ROLES_APPLICATIONS_FK` (`id_APPLICATIONS`);

--
-- Index pour la table `ROLES_PROFILS`
--
ALTER TABLE `ROLES_PROFILS`
  ADD PRIMARY KEY (`id_PROFILS`,`id_ROLES`),
  ADD KEY `ROLES_PROFILS_ROLES0_FK` (`id_ROLES`);

--
-- Index pour la table `UTILISATEURS`
--
ALTER TABLE `UTILISATEURS`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `UTILISATEURS_ROLES`
--
ALTER TABLE `UTILISATEURS_ROLES`
  ADD PRIMARY KEY (`id_ROLES`,`id_UTILISATEURS`),
  ADD KEY `UTILISATEURS_ROLES_UTILISATEURS0_FK` (`id_UTILISATEURS`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `APPLICATIONS`
--
ALTER TABLE `APPLICATIONS`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `PROFILS`
--
ALTER TABLE `PROFILS`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `ROLES`
--
ALTER TABLE `ROLES`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `UTILISATEURS`
--
ALTER TABLE `UTILISATEURS`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `PROFILS`
--
ALTER TABLE `PROFILS`
  ADD CONSTRAINT `PROFILS_APPLICATIONS_FK` FOREIGN KEY (`id_APPLICATIONS`) REFERENCES `APPLICATIONS` (`id`);

--
-- Contraintes pour la table `ROLES`
--
ALTER TABLE `ROLES`
  ADD CONSTRAINT `ROLES_APPLICATIONS_FK` FOREIGN KEY (`id_APPLICATIONS`) REFERENCES `APPLICATIONS` (`id`);

--
-- Contraintes pour la table `ROLES_PROFILS`
--
ALTER TABLE `ROLES_PROFILS`
  ADD CONSTRAINT `ROLES_PROFILS_PROFILS_FK` FOREIGN KEY (`id_PROFILS`) REFERENCES `PROFILS` (`id`),
  ADD CONSTRAINT `ROLES_PROFILS_ROLES0_FK` FOREIGN KEY (`id_ROLES`) REFERENCES `ROLES` (`id`);

--
-- Contraintes pour la table `UTILISATEURS_ROLES`
--
ALTER TABLE `UTILISATEURS_ROLES`
  ADD CONSTRAINT `UTILISATEURS_ROLES_ROLES_FK` FOREIGN KEY (`id_ROLES`) REFERENCES `ROLES` (`id`),
  ADD CONSTRAINT `UTILISATEURS_ROLES_UTILISATEURS0_FK` FOREIGN KEY (`id_UTILISATEURS`) REFERENCES `UTILISATEURS` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
