<?php

include_once(realpath(dirname(__FILE__).'/../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

$app->get(API_URL_PREFIX . '/admin/apps/{id}', function ($id) use ($app, $twig) {

    $req = Request::createFromGlobals();

    $result = false;
    $html = false;
    $rolesLocal = $rolesNotLocal = $roles_deleted = array();

    $app = new T_APPLICATION();
    $app->getFromDbById($id);

    if($app->getId() > 0){

        $rolesLocal = $app->setAppsRolesLocalInBDD();

        $template = $twig->loadTemplate('form_app.twig');
        $params = array(
            'app' => $app
        );
        $html = $template->render($params);
        $result = true;

    }

    echo json_encode(array(
        'result' => $result,
        'html' => $html
    ));
    exit;

});
$app->post(API_URL_PREFIX.'/admin/apps/{id}/edit', function($id) use ($app){

    $req = Request::createFromGlobals();
    $result = false;

    $app = new T_APPLICATION();
    $app->getFromDbById($id);
    $app->statut = $req->request->has('statut') ? $req->request->get('statut') : T_APPLICATION::INACTIVE;
    $result = $app->update();

    echo json_encode(array(
        'result' => $result
    ));
    exit;

});

$app->post(API_URL_PREFIX.'/admin/apps/roles/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'id,tag,descr,tag_local_exist,id_APPLICATIONS';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult();

    echo json_encode($result->read('ROLES', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->get(API_URL_PREFIX . 'admin/apps/{app_id}/roles/{id}', function ($app_id,$id) use ($app, $twig) {

    $req = Request::createFromGlobals();

    $result = $html = false;

    $role = new T_ROLE();
    $role->getFromDbById($id);

    if($role->getId() > 0){

        $template = $twig->loadTemplate('form_role.twig');
        $params = array(
            'role' => $role
        );
        $html = $template->render($params);
        $result = true;

    }

    echo json_encode(array(
        'result' => $result,
        'html' => $html,
    ));
    exit;

});

$app->post(API_URL_PREFIX.'/admin/apps/profils/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'id,nom,id_APPLICATIONS';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult();

    echo json_encode($result->read('PROFILS', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->post(API_URL_PREFIX.'/admin/apps/{id}/profils/add', function($id) use ($app){

    $req = Request::createFromGlobals();
    $result = false;

    $profil = new T_PROFIL();
    $profil->nom = $req->request->has('nom') ? $req->request->get('nom') : '';
    $profil->id_APPLICATIONS = $id;
    $result = $profil->insert();

    echo json_encode(array(
        'result' => $result,
    ));
    exit;

});
$app->get(API_URL_PREFIX . 'admin/apps/{app_id}/profils/{id}', function ($app_id,$id) use ($app, $twig) {

    $req = Request::createFromGlobals();

    $result = $html = false;

    $profil = new T_PROFIL();
    $profil->getFromDbById($id);

    if($profil->getId() > 0){

        $roles = new T_ROLE();
        $roles = $roles->getAllData("id_APPLICATIONS = '{$app_id}'");
        foreach ($roles as $key => $value){
            $roles[$key] = $value->__toArray();
            $role = new T_ROLE_PROFIL();
            $role = $role->getAllData("id_PROFILS = '{$id}' AND id_ROLES = '{$value->id}'");
            $roles[$key]['selected'] = (count($role) === 0) ? '' : 'selected="selected"';
        }

        $template = $twig->loadTemplate('form_profil.twig');
        $params = array(
            'profil' => $profil,
            'roles' => $roles
        );
        $html = $template->render($params);
        $result = true;

    }

    echo json_encode(array(
        'result' => $result,
        'html' => $html,
    ));
    exit;

});
$app->post(API_URL_PREFIX . 'admin/apps/{app_id}/profils/{id}/edit', function ($app_id,$id) use ($app, $twig) {

    $req = Request::createFromGlobals();

    $result = false;
    $roles = $req->request->has('roles') ? $req->request->get('roles') : array();

    TDatabase::excuteSqlQuery("DELETE FROM `ROLES_PROFILS` WHERE `ROLES_PROFILS`.`id_PROFILS` = {$id}");

    foreach ($roles as $roleID){
        $roleBDD = new T_ROLE_PROFIL();
        $roleBDD->id_PROFILS = $id;
        $roleBDD->id_ROLES = $roleID;
        $roleBDD->insert();
    }

    $profil = new T_PROFIL();
    $profil->getFromDbById($id);
    $profil->nom = $req->request->has('nom') ? $req->request->get('nom') : '';
    $result = $profil->save();

    echo json_encode(array(
        'result' => $result
    ));
    exit;

});
$app->delete(API_URL_PREFIX . 'admin/apps/{app_id}/profils/{id}/delete', function ($app_id,$id) use ($app, $twig) {

    $req = Request::createFromGlobals();

    TDatabase::excuteSqlQuery("DELETE FROM `ROLES_PROFILS` WHERE `ROLES_PROFILS`.`id_PROFILS` = {$id}");
    TDatabase::excuteSqlQuery("DELETE FROM `PROFILS` WHERE `PROFILS`.`id` = {$id}");

    echo json_encode(array(
        'result' => true
    ));
    exit;

});