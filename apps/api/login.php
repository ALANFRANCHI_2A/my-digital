<?php

use Symfony\Component\HttpFoundation\Request;

$app->post(API_URL_PREFIX . '/login', function () use ($app) {

    $req = Request::createFromGlobals();

    $result = false;

    $email = $req->request->get('email');
    $password = $req->request->get('password');

    $application = new TApplication();
    $user = new T_UTILISATEUR();
    $user->getFromDbByProperty('email', $email);

    if($user->getId() > 0 && password_verify($password, $user->mdp)){
        $application->setValue("app.ca.mydigital.user.logged",$user);
        $result = true;
    }

    echo json_encode(array(
        'result' => $result
    ));
    exit;

});
$app->post(API_URL_PREFIX . '/login/mdp_oublie', function () use ($app,$twig) {

    $req = Request::createFromGlobals();

    $result = false;

    $email = $req->request->get('email');

    $user = new T_UTILISATEUR();
    $user->getFromDbByProperty('email', $email);

    if($user->getId() > 0){

        $template = $twig->loadTemplate('email_mdp_oublie.twig');
        $params = array(
            'user' => $user
        );

        $mail = new TMailManager();
        $mail->mailer->AddEmbeddedImage(realpath(dirname(__FILE__).'/../img/header_email-mydigital.png'), 'header_mydigital', 'header_email-mydigital.png');
        $mail->mailer->AddEmbeddedImage(realpath(dirname(__FILE__).'/../img/footer_email.png'), 'footer', 'footer_email.png');
        $mail->sendEmail('alexandre.lanfranchi@icloud.com','','','[MYDIGITAL] : Réinitialisation de votre mot de passe',$template->render($params));
        //$mail->sendEmail($user->email,'','','[MYDIGITAL] : Réinitialisation de votre mot de passe',$template->render($params));

        $result = true;
    }

    echo json_encode(array(
        'result' => $result
    ));
    exit;

});
$app->post(API_URL_PREFIX . '/login/mdp_reset', function () use ($app,$twig) {

    $req = Request::createFromGlobals();

    $result = false;

    $email = $req->request->get('email');

    $user = new T_UTILISATEUR();
    $user->getFromDbByProperty('email', $email);

    if($user->getId() > 0){

        $template = $twig->loadTemplate('email_mdp_oublie.twig');
        $params = array(
            'user' => $user
        );

        $mail = new TMailManager();
        $mail->mailer->AddEmbeddedImage(realpath(dirname(__FILE__).'/../img/header_email-mydigital.png'), 'header_mydigital', 'header_email-mydigital.png');
        $mail->mailer->AddEmbeddedImage(realpath(dirname(__FILE__).'/../img/footer_email.png'), 'footer', 'footer_email.png');
        $mail->sendEmail('alexandre.lanfranchi@icloud.com','','','[MYDIGITAL] : Réinitialisation de votre mot de passe',$template->render($params));
        //$mail->sendEmail($user->email,'','','[MYDIGITAL] : Réinitialisation de votre mot de passe',$template->render($params));

        $result = true;
    }

    echo json_encode(array(
        'result' => $result
    ));
    exit;

});

$app->post(API_URL_PREFIX.'/login/mdp_reset/{guid}', function($guid) use ($app){

    $req = Request::createFromGlobals();
    $result = false;

    $pwd = $req->request->has('mdp') ? $req->request->get('mdp') : '';

    $user = new T_UTILISATEUR();
    $user->getFromDbByProperty('guid', $guid);

    if($user->getId() > 0){
        $user->mdp = password_hash($pwd, PASSWORD_BCRYPT);
        $result = $user->save();
    }

    echo json_encode(array(
        'result' => $result
    ));
    exit;

});