<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");

include_once(realpath(dirname(__FILE__).'/../model/datamodel.php'));

use Symfony\Component\Finder\Finder;

define('API_URL_PREFIX', '');

$app = new Silex\Application();
$app['debug'] = true;
error_reporting(0);

$finder = new Finder();
$finder->files()->in(dirname(__FILE__).'/../api/')->name('*.php');

foreach ($finder as $file) {
    $realPath = $file->getRealpath();
    $filename = basename($realPath);
    if ($filename != 'index.php') {
        include_once($realPath);
    }
}

$app->run();