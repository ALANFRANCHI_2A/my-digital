<?php

include_once(realpath(dirname(__FILE__).'/../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

$app->post(API_URL_PREFIX.'/admin/users/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'guid,nom,prenom,email,statut';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult();

    echo json_encode($result->read('UTILISATEURS', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->post(API_URL_PREFIX.'/admin/users/add', function() use ($app, $twig){

    $req = Request::createFromGlobals();
    $result = false;

    $user = new T_UTILISATEUR();
    $user->nom = $req->request->has('nom') ? $req->request->get('nom') : '';
    $user->prenom = $req->request->has('prenom') ? $req->request->get('prenom') : '';
    $user->email = $req->request->has('email') ? $req->request->get('email') : '';
    $user->insert();

    $template = $twig->loadTemplate('email_activation_compte.twig');
    $params = array(
        'user' => $user
    );

    $mail = new TMailManager();
    $mail->mailer->AddEmbeddedImage(realpath(dirname(__FILE__).'/../img/header_email-mydigital.png'), 'header_mydigital', 'header_email-mydigital.png');
    $mail->mailer->AddEmbeddedImage(realpath(dirname(__FILE__).'/../img/footer_email.png'), 'footer', 'footer_email.png');
    $mail->sendEmail($user->email,'','','[MYDIGITAL] : Activation de votre compte',$template->render($params));

    echo json_encode(array(
        'result' => $result
    ));
    exit;

});
$app->post(API_URL_PREFIX.'/admin/users/active/{guid}', function($guid) use ($app){

    $req = Request::createFromGlobals();
    $result = false;

    $pwd = $req->request->has('mdp') ? $req->request->get('mdp') : '';

    $user = new T_UTILISATEUR();
    $user->getFromDbByProperty('guid', $guid);

    if($user->getId() > 0){
        $user->mdp = password_hash($pwd, PASSWORD_BCRYPT);
        $user->statut = T_UTILISATEUR::ACTIVE;
        $result = $user->save();
        $application = new TApplication();
        $application->setValue("app.ca.mydigital.user.logged",$user);
    }

    echo json_encode(array(
        'result' => $result
    ));
    exit;

});
$app->get(API_URL_PREFIX . '/admin/users/{guid}', function ($guid) use ($app, $twig) {

    $req = Request::createFromGlobals();

    $result = false;
    $html = false;

    $user = new T_UTILISATEUR();
    $user->getFromDbByProperty('guid', $guid);

    if($user->getId() > 0){

        $rolesApps = array();
        $rolesAppsId = array();
        $apps = new T_APPLICATION();
        $apps = $apps->getAllData();
        foreach ($apps as $app){

            $rolesApp = array();
            $rolesApp['app'] = $app;
            $rolesAppsId[] = "app-{$app->getId()}";

            $roles = new T_ROLE();
            $rolesApp['roles'] = $roles->getAllData("id_APPLICATIONS = {$app->getId()}");
            foreach ($rolesApp['roles'] as $role)
                $rolesAppsId[] = "app-{$app->getId()}-role-{$role->getId()}";

            $profils = new T_PROFIL();
            $profils = $profils->getAllData("id_APPLICATIONS = {$app->getId()}");
            $rolesApp['profils'] = array();
            foreach ($profils as $profil){
                $roles = new T_ROLE();
                $sql = "SELECT id_ROLES FROM ROLES_PROFILS WHERE ROLES.id = ROLES_PROFILS.id_ROLES AND ROLES_PROFILS.id_PROFILS = {$profil->getId()}";
                $roles = $roles->getAllData("id_APPLICATIONS = {$app->getId()} AND id IN ({$sql})");
                $rolesApp['profils'][] = array(
                    'profil' => $profil,
                    'roles' => $roles
                );
                $rolesAppsId[] = "app-{$app->getId()}-profil-{$profil->getId()}";
                foreach ($roles as $role)
                    $rolesAppsId[] = "app-{$app->getId()}-profil-{$profil->getId()}-role-{$role->getId()}";
            }

            $rolesApps[] = $rolesApp;

        }

        $template = $twig->loadTemplate('form_utilisateur.twig');
        $params = array(
            'user' => $user,
            'roles_apps' => $rolesApps,
            'user_logged' => TApplication::getUserLogged()
        );
        $html = $template->render($params);
        $result = true;
    }

    echo json_encode(array(
        'result' => $result,
        'html' => $html,
        'roles_apps_id' => $rolesAppsId,
        'user_roles' => $user->getRolesUsers()

    ));
    exit;

});
$app->post(API_URL_PREFIX . '/admin/users/{guid}', function ($guid) use ($app, $twig) {

    $req = Request::createFromGlobals();
    $result = false;

    $user = new T_UTILISATEUR();
    $user->getFromDbByProperty('guid', $guid);

    if($user->getId() > 0){

        $user->statut = $req->request->has('statut') ? $req->request->get('statut') : -1;
        TDatabase::excuteSqlQuery("DELETE FROM `UTILISATEURS_ROLES` WHERE `UTILISATEURS_ROLES`.`id_UTILISATEURS` = {$user->getId()}");
        $roles = $req->request->has('roles') ? $req->request->get('roles') : '';
        $roles = explode(',', $roles);

        foreach ($roles as $roleId){
            $role_user = new T_UTILISATEUR_ROLE();
            $role_user->id_ROLES = $roleId;
            $role_user->id_UTILISATEURS = $user->getId();
            $role_user->insert();
        }

        $result = $user->save();

    }

    echo json_encode(array(
        'result' => $result,
        'roles' => $roles,
    ));
    exit;

});