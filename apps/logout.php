<?php

include_once(realpath(dirname(__FILE__).'/model/datamodel.php'));

use Symfony\Component\HttpFoundation\RedirectResponse;

$application = new TApplication();
$application->session->invalidate();

$response = new RedirectResponse("connexion.php");
$response->send();