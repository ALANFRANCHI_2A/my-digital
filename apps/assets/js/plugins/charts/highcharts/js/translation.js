Highcharts.setOptions({
    lang: {
        months: [
            getLangValue("date.january"),
            getLangValue("date.february"),
            getLangValue("date.march"),
            getLangValue("date.april"),
            getLangValue("date.may"),
            getLangValue("date.june"),
            getLangValue("date.july"),
            getLangValue("date.august"),
            getLangValue("date.september"),
            getLangValue("date.october"),
            getLangValue("date.november"),
            getLangValue("date.december")
        ],
        weekdays: [
            getLangValue("date.sunday"),
            getLangValue("date.monday"),
            getLangValue("date.tuesday"),
            getLangValue("date.wednesday"),
            getLangValue("date.thursday"),
            getLangValue("date.friday"),
            getLangValue("date.saturday")
        ],
        shortMonths: [
            getLangValue("date.jan"),
            getLangValue("date.feb"),
            getLangValue("date.mar"),
            getLangValue("date.apr"),
            getLangValue("date.may_short"),
            getLangValue("date.jun"),
            getLangValue("date.jul"),
            getLangValue("date.aug"),
            getLangValue("date.sept"),
            getLangValue("date.oct"),
            getLangValue("date.nov"),
            getLangValue("date.dec")
        ],
        downloadPNG: getLangValue("libraries.highcharts.download_png_image"),
        downloadJPEG: getLangValue("libraries.highcharts.download_jpeg_image"),
        downloadPDF: getLangValue("libraries.highcharts.download_pdf_document"),
        downloadSVG: getLangValue("libraries.highcharts.download_vector_document"),
        exportButtonTitle: getLangValue("libraries.highcharts.graph_export"),
        loading: getLangValue("libraries.highcharts.loading"),
        noData: getLangValue("libraries.highcharts.no_data"),
        printChart: getLangValue("libraries.highcharts.print_chart"),
        printButtonTitle: getLangValue("libraries.highcharts.print_chart"),
        resetZoom: getLangValue("libraries.highcharts.reset_zoom"),
        resetZoomTitle: getLangValue("libraries.highcharts.reset_zoom_level"),
        decimalPoint: ',',
        thousandsSep: ' '
    }
});