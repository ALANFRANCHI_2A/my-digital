$(function () {

    var currentPage = document.location.href.substring(document.location.href.lastIndexOf( "/" )+1 );

    if(currentPage == "")
        currentPage = "./";

    $("#navbar li").removeClass("active"); // Remove All Class Active

    $("#navbar li a[href='"+currentPage+"']").parent().addClass("active"); // Add Class Active New Element

    if($("#navbar li a[href='"+currentPage+"']").parent().parent().parent().hasClass("dropdown"))
        // Add Class Active New Element IF SUB MENU
        $("#navbar li a[href='"+currentPage+"']").parent().parent().parent().addClass("active");

});