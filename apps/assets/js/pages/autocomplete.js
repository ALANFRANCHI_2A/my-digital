function initAutocomplete(selector, table, field) {

    $(selector).easyAutocomplete({
        url: function(keyword) {
            return "api/comptes/autocomplete";
        },
        getValue: function(element) {
            return element[0];
        },
        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: {
                dataType: "json"
            }
        },
        preparePostData: function(data) {
            if($(selector).val() != ''){
                data.type = "easyautocomplete";
                data.table = table;
                data.field = field;
                data.keyword = $(selector).val();
                return data;
            }
        },
        requestDelay: 400
    });

    /*$(selector).next().click(function () {
        var btn = $(selector).next().children();
        if (btn.attr("disabled") === undefined || btn.attr("disabled") !== 'disabled') {
            if ($(selector).autocomplete("widget").is(":visible")) {
                $(selector).autocomplete("close");
            } else {
                $(selector).autocomplete("search", $(selector).val());
            }
        }
    });*/

}