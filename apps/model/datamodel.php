<?php

require_once(realpath(dirname(__FILE__).'/../symfony/vendor/autoload.php'));
include_once(realpath(dirname(__FILE__).'/../inc/twiginit.php'));
require_once(realpath(dirname(__FILE__).'/auth.php'));
require_once(realpath(dirname(__FILE__).'/../inc/utils.php'));
include_once(realpath(dirname(__FILE__).'/../lib/logger/logmanager.php'));

use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

date_default_timezone_set('Europe/Paris');

class ObjectModelCore{

    const TYPE_INT     = 1;
    const TYPE_BOOL    = 2;
    const TYPE_STRING  = 3;
    const TYPE_FLOAT   = 4;
    const TYPE_DATE    = 5;
    const TYPE_HTML    = 6;
    const TYPE_NOTHING = 7;
    const TYPE_SQL     = 8;

    protected $definition = array();

    public function __construct($id = null){

        $result = array();
        $identifier = $this->definition["identifier"];
        $fields = $this->definition['fields'];

        $this->{$identifier} = 0;
        foreach ($fields as $field => $options) {
            $this->{$field} = '';
            if (array_key_exists('type', $options)) {
                if ($options['type'] == self::TYPE_INT) $this->{$field} = 0;
                if ($options['type'] == self::TYPE_FLOAT) $this->{$field} = 0;
                if ($options['type'] == self::TYPE_BOOL) $this->{$field} = 0;
                if ($options['type'] == self::TYPE_DATE) $this->{$field} = null;
            }
        }

        return $result;

    }

    public function __toArray(){

        $result = array();
        $identifier = $this->definition["identifier"];
        $fields = $this->definition['fields'];

        $result[$identifier] = $this->{$identifier};
        foreach ($fields as $field => $options) {
            $result[$field] = $this->{$field};
        }

        return $result;

    }

    public function getId(){

        $identifier = $this->definition["identifier"];
        return $this->{$identifier};

    }

	public function getTableName(){
        return $this->definition['table'];
    }

    public function getClassName(){
        return get_class($this);
    }

    public function getDatabaseConnect(){
        $class = new \ReflectionClass($this->getClassName());
        $filename = $class->getFileName();
        if(strpos($filename, 'modules') !== FALSE){
            $moduleFolder = dirname($filename, 3);
            $appConfig = new TAppConfig($moduleFolder);
            if(!empty($appConfig->nom_bdd))
                return TDatabase::connect($appConfig->nom_bdd);
        }
        return TDatabase::connect();
    }

    public function getFields(){

        $result = array();
        $fields = $this->definition['fields'];

        foreach ($fields as $field => $options) {
            $result[] = array(
                'field' => $field,
                'options' => $options,
            );
        }

        return $result;

    }

    public function getFromDbById($id, $dbHandle = null){
        $result = false;
        try {
            $dbHnd = ($dbHandle === null) ? self::getDatabaseConnect() : $dbHandle;
            if ($dbHnd != null) {
                $sqlReq = 'SELECT * FROM '.$this->getTableName().' WHERE '.$this->definition["identifier"].' = :id';
                getLogger(LOG_FILE_SQL)->info("[".$this->getClassName()."::getFromDbById(".$id.")] SQL-REQ: [$sqlReq]");
                $stmt = $dbHnd->prepare($sqlReq);
                $stmt->setFetchMode(PDO::FETCH_INTO, $this);
                $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                $stmt->execute();
                $res = $stmt->fetch();
                if (!($res == false)) $result = true;
            }
            return $result;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            getLogger()->error("[".$this->getClassName()."::getFromDbById(".$id.")] SQL-ERR: [{$e->getMessage()}]");
        }
        return $result;
    }

    public function TWIG_getFromDbById($id, $dbHandle = null){
        $result = false;
        try {
            $dbHnd = ($dbHandle === null) ? self::getDatabaseConnect() : $dbHandle;
            if ($dbHnd != null) {
                $sqlReq = 'SELECT * FROM '.$this->getTableName().' WHERE '.$this->definition["identifier"].' = :id';
                getLogger(LOG_FILE_SQL)->info("[".$this->getClassName()."::getFromDbById(".$id.")] SQL-REQ: [$sqlReq]");
                $stmt = $dbHnd->prepare($sqlReq);
                $stmt->setFetchMode(PDO::FETCH_INTO, $this);
                $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                $stmt->execute();
                $res = $stmt->fetch();
                if (!($res == false)) $result = true;
            }
            return $this;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            getLogger()->error("[".$this->getClassName()."::getFromDbById(".$id.")] SQL-ERR: [{$e->getMessage()}]");
        }
        return $this;
    }

    public function getFromDbByProperty($property, $value, $dbHandle = null, $ignoreCase = false){
        $result = false;
        try {
            $dbHnd = ($dbHandle === null) ? self::getDatabaseConnect() : $dbHandle;
            if ($dbHnd != null) {
                if ($ignoreCase) {
                    $sqlWhere = "UPPER($property) LIKE UPPER(:propvalue)";
                } else {
                    $sqlWhere = "$property LIKE :propvalue";
                }
                $sqlReq = 'SELECT * FROM '.$this->getTableName().' WHERE '.$sqlWhere;
                getLogger(LOG_FILE_SQL)->info("[".$this->getClassName()."::getFromDbByProperty($property, $value)] SQL-REQ: [$sqlReq]");
                $stmt = $dbHnd->prepare($sqlReq);
                $stmt->setFetchMode(PDO::FETCH_INTO, $this);
                $stmt->bindParam(':propvalue', $value, PDO::PARAM_STR);
                $stmt->execute();
                $res = $stmt->fetch();
                if (!($res == false)) $result = true;
            }
            return $result;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            getLogger()->error("[".$this->getClassName()."::getFromDbByProperty($property, $value)] SQL-ERR: [{$e->getMessage()}]");
        }
        return $result;
    }

    public function save($dbHandle = null){
        return ($this->getId() === 0 || $this->getId() === "") ? $this->insert($dbHandle) : $this->update($dbHandle);
    }

    public function insert($dbHandle = null){

        $result = false;
        try {
            $dbHnd = ($dbHandle === null) ? self::getDatabaseConnect() : $dbHandle;
            if ($dbHnd != null) {

                // Automatically fill dates
                if (property_exists($this, 'created_date'))
                    $this->created_date = date('Y-m-d H:i:s');

                $fields = $this->definition['fields'];
                $fieldsCount = count($fields);
                $identifier = $this->definition["identifier"];
                $fieldsSqlString = ($fieldsCount > 0) ? "$identifier, " : "$identifier";
                $valuesSqlString = ($fieldsCount > 0) ? ":$identifier, " : ":$identifier";

                $i = 0;
                foreach ($fields as $field => $options) {
                    $fieldsSqlString .= "$field";
                    if ($i < $fieldsCount - 1) $fieldsSqlString .= ', ';

                    $valuesSqlString .= ":$field";
                    if ($i < $fieldsCount - 1) $valuesSqlString .= ', ';

                    $i++;
                }

                $sqlReq = 'INSERT INTO '.$this->getTableName().' ('.$fieldsSqlString.') values ('.$valuesSqlString.')';
                getLogger(LOG_FILE_SQL)->info("[{$this->getClassName()}::insert(".$this->getId().")] SQL-REQ: [$sqlReq]");

                $stmt = $dbHnd->prepare($sqlReq);
                //$this->{$identifier} = ($this->getId() > 0) ? $this->getId() : 0;
                if ($stmt->execute($this->__toArray($this))) {
                    $resultId = $dbHnd->lastInsertId($identifier);
                    $this->{$identifier} = ($resultId == 0) ? $this->getId() : $resultId;
                    $result = true;
                }

            }
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            getLogger()->error("[{$this->getClassName()}::insert(".$this->getId().")] SQL-ERR: [{$e->getMessage()}] -- SQL-REQ: [$sqlReq]");
        }

        if (!$result)
            return false;

        return $result;

    }

    public function update($dbHandle = null){

        $result = false;
        try {
            $dbHnd = ($dbHandle === null) ? self::getDatabaseConnect() : $dbHandle;
            if ($dbHnd != null) {

                // Automatically fill dates
                if (property_exists($this, 'updated_date'))
                    $this->updated_date = date('Y-m-d H:i:s');

                $fields = $this->definition['fields'];
                $fieldsCount = count($fields);
                $identifier = $this->definition["identifier"];
                $fieldsValues = ($fieldsCount > 0) ? "$identifier = :$identifier, " : "$identifier = :$identifier";

                $i = 0;
                foreach ($fields as $field => $options) {
                    $fieldsValues .= "$field = :$field";
                    if ($i < $fieldsCount - 1) $fieldsValues .= ', ';
                    $i++;
                }

                $sqlReq = 'UPDATE '.$this->getTableName().' SET '.$fieldsValues.' WHERE ('.$identifier.' = :'.$identifier.')';
                getLogger(LOG_FILE_SQL)->info("[{$this->getClassName()}::update(".$this->getId().")] SQL-REQ: [$sqlReq]");

                $stmt = $dbHnd->prepare($sqlReq);
                if ($stmt->execute($this->__toArray($this)))
                    $result = true;

            }
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            getLogger()->error("[{$this->getClassName()}::update(".$this->getId().")] SQL-ERR: [{$e->getMessage()}] -- SQL-REQ: [$sqlReq]");
        }

        if (!$result)
            return false;

        return $result;

    }

    public function delete($dbHandle = null){

        $result = false;
        try {
            $dbHnd = ($dbHandle === null) ? self::getDatabaseConnect() : $dbHandle;
            if ($dbHnd != null) {

                $identifier = $this->definition["identifier"];

                $sqlReq = 'DELETE FROM '.$this->getTableName().' WHERE '.$identifier.' = ":id"';
                getLogger(LOG_FILE_SQL)->info("[{$this->getClassName()}::delete(".$this->getId().")] SQL-REQ: [$sqlReq]");
                $stmt = $dbHnd->prepare($sqlReq);
                $stmt->bindParam(':id', $this->{$identifier}, PDO::PARAM_INT);
                if ($stmt->execute())
                    $result = true;

            }
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            getLogger()->error("[{$this->getClassName()}::delete(".$this->getId().")] SQL-ERR: [{$e->getMessage()}]");
        }

        if (!$result)
            return false;

        return $result;

    }

    public function getAllData($criteria = null, $columnOrder = null, $dbHandle = null){

        $result = array();
        try {
            $dbHnd = ($dbHandle === null) ? self::getDatabaseConnect() : $dbHandle;
            if ($dbHnd != null) {

                $sqlReq = 'SELECT * FROM '.$this->getTableName();
                if ($criteria != '') {
                    $sqlReq .= " WHERE $criteria";
                }
                if ($columnOrder != '') {
                    $sqlReq .= " ORDER BY $columnOrder";
                }
                getLogger(LOG_FILE_SQL)->info("[{$this->getClassName()}::getAll(".$criteria.")] SQL-REQ: [$sqlReq]");
                $stmt = $dbHnd->prepare($sqlReq);
                $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->getClassName());
                $stmt->execute();
                $result = $stmt->fetchAll();

            }
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            getLogger()->error("[{$this->getClassName()}::getAll(".$criteria.")] SQL-ERR: [{$e->getMessage()}]");
        }

        return $result;

    }

}

class ObjectModel extends ObjectModelCore{

    public function __construct($id = null){
        parent::__construct($id);
    }

}

class TApplication {

    var $session;
    var $twig;

    public function __construct(){
        $this->session = self::getSession();
    }

    public static function getSession(){
        $session = new Session();
        if(session_status() != \PHP_SESSION_ACTIVE) $session->start();
        return $session;
    }

    public function isValueExists($name){
        return $this->session->has($name);
    }

    public function setValue($name, $value){
        return $this->session->set($name, $value);
    }

    public function getValue($name){
        return $this->session->get($name);
    }

    public function remove($name){
        return $this->session->remove($name);
    }

    public function setTwig($twig){
        $this->twig = $twig;
        return $this->twig;
    }

    public function getTwig(){
        return $this->twig;
    }

    public static function accessDenied() {
        header("HTTP/1.1 403 Forbidden");
        exit(0);
    }

    public static function requireAuth() {
        $session = self::getSession();
        if (!$session->has('app.ca.mydigital.user.logged')) {
            $response = new RedirectResponse(self::getUrlPrefix().'/connexion.php');
            $response->send();
            exit(0);
        }
        return $session;
    }

    public static function getUrlPrefix(){
        return explode('apps', Request::createFromGlobals()->getBasePath())[0].'apps';
    }

    public static function getUserLogged() {
        $req = Request::createFromGlobals();
        $session = self::getSession();
        $user = $session->get('app.ca.mydigital.user.logged');
        $navbar_apps = array();
        $apps = new T_APPLICATION();
        $apps = $apps->getAllData('statut = 1 AND nom_dossier != "apps"');
        foreach ($apps as $app){
            if($user->hasRole('ACCESS', $app->nom_dossier))
                $navbar_apps[] = $app;
        }
        return  array(
            'user' => $user,
            'navbar_apps' => $navbar_apps,
            'current_module' => basename($req->getBasePath())
        );
    }

}

class TDatabase{

    public static function connect($dbname = null){
        global $cfg;
		$dbname = ($dbname === null) ? $cfg['database'] : $dbname;
        $dbHnd = new PDO('mysql:host=' . $cfg['host'] . ';port='. $cfg['port'] .';dbname=' . $dbname . ';charset=utf8mb4', $cfg['username'], $cfg['password']);
        $dbHnd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dbHnd;
    }

    public static function excuteSqlQuery($sqlReq, $dbHandle = null) {

        $result = false;
        try {
            $dbHnd = ($dbHandle === null) ? TDatabase::connect() : $dbHandle;
            if ($dbHnd != null) {
                getLogger(LOG_FILE_SQL)->info("[TDatabase::excuteSqlQuery($sqlReq)] SQL-REQ: [$sqlReq]");
                $dbHnd->query($sqlReq);
                $result = true;
            }
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            getLogger()->error("[TDatabase::excuteSqlQuery($sqlReq)] SQL-ERR: [{$e->getMessage()}]");
            $result = false;
        }

        return $result;

    }

    public static function TWIG_excuteSqlQuery($sqlReq, $dbHandle = null) {

        $result = false;
        try {
            $dbHnd = ($dbHandle === null) ? TDatabase::connect() : $dbHandle;
            if ($dbHnd != null) {
                getLogger(LOG_FILE_SQL)->info("[TDatabase::TWIG_excuteSqlQuery($sqlReq)] SQL-REQ: [$sqlReq]");
                return $dbHnd->query($sqlReq);
            }
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            getLogger()->error("[TDatabase::excuteSqlQuery($sqlReq)] SQL-ERR: [{$e->getMessage()}]");
            $result = false;
        }

        return $result;

    }

    public static function getDataAssoc($table = '',  $criteria = '', $fields = "*", $dbHandle = null, $keyPair = false, $leftjoin = array(), $innerjoin = array()){

        $result = array();
        try {
            $dbHnd = ($dbHandle === null) ? TDatabase::connect() : $dbHandle;
            if ($dbHnd != null) {
                if ($table != '') {
                    $sqlReq = "SELECT $fields FROM $table ";
                    if (count($leftjoin) > 0) {
                        foreach ($leftjoin as $lj)
                            $sqlReq .= " LEFT JOIN $lj";
                    }
                    if (count($innerjoin) > 0) {
                        foreach ($innerjoin as $ij)
                            $sqlReq .= " INNER JOIN $ij";
                    }
                    if ($criteria != '') {
                        $sqlReq .= " WHERE $criteria";
                    }
                    getLogger(LOG_FILE_SQL)->info("SQL-REQ: [$sqlReq]");
                    $stmt = $dbHnd->prepare($sqlReq);
                    $stmt->setFetchMode(PDO::FETCH_ASSOC);
                    $stmt->execute();
                    if($keyPair){
                        $result = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
                    }else{
                        $result = $stmt->fetchAll();
                    }
                }
            }

        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            getLogger()->error("SQL-ERR: [{$e->getMessage()}]");
            $result = null;
        }

        return $result;

    }

    public static function getData($table = '', $fields = "*", $criteria = '', $leftjoin = array(), $innerjoin = array(), $method = PDO::FETCH_ASSOC, $dbHandle = null, $className = null){
        $result = array();
        try {
            $dbHnd = ($dbHandle === null) ? TDatabase::connect() : $dbHandle;
            //$dbHnd = TDatabase::connect();
            if ($dbHnd != null) {
                if ($table != '') {
                    $sqlReq = "SELECT $fields FROM $table ";
                    if (count($leftjoin) > 0) {
                        foreach ($leftjoin as $lj)
                            $sqlReq .= " LEFT JOIN $lj";
                    }
                    if (count($innerjoin) > 0) {
                        foreach ($innerjoin as $ij)
                            $sqlReq .= " INNER JOIN $ij";
                    }
                    if ($criteria != '') {
                        $sqlReq .= " WHERE $criteria";
                    }
                    $stmt = $dbHnd->prepare($sqlReq);
                    if($method == PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE && $className != null){
                        $stmt->setFetchMode($method, $className);
                    }else{
                        $stmt->setFetchMode($method);
                    }
                    $stmt->execute();
                    $result = $stmt->fetchAll();
                }
            }
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            $result = null;
        }
        return $result;
    }
    /*public static function getDataAssoc($table = '', $fields = "*", $criteria = '', $leftjoin = array(), $innerjoin = array(), $dbHandle = false){
        return TDatabase::getData($table, $fields, $criteria, $leftjoin, $innerjoin, PDO::FETCH_ASSOC, $dbHandle);
    }*/
    public static function getDataKeyPair($table = '', $fields = "*", $criteria = '', $leftjoin = array(), $innerjoin = array(), $dbHandle = null){
        return TDatabase::getData($table, $fields, $criteria, $leftjoin, $innerjoin, PDO::FETCH_KEY_PAIR, $dbHandle);
    }
    public static function getDataClass($table = '', $fields = "*", $criteria = '', $leftjoin = array(), $innerjoin = array(), $dbHandle = null, $className = null){
        return TDatabase::getData($table, $fields, $criteria , $leftjoin, $innerjoin, PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $dbHandle, $className);
    }

}

class TFunction{

    public static function var_dump($var){
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
    }

    public static function unsetArray($array, $inArray){
        foreach ($array as $index => $var){
            if(in_array($var, $inArray)){
                unset($array[$index]);
            }
        }
        return $array;
    }

	public static function getJson($file){
	    $string = file_get_contents($file);
		return json_decode($string, true);
    }

	public static function GUID(){
    	if (function_exists('com_create_guid') === true)
        	return trim(com_create_guid(), '{}');
	    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
	}

    public static function getUniqueTime() {
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
        return $d->format("Ymd_Hisu");
    }

}

class TMailManager {

    var $mailer;

    public function __construct(){

        $this->mailer = new PHPMailer(true);
        $this->mailer->isSendmail();

        //$this->mailer->SMTPDebug  = 2;

        $this->mailer->CharSet = "UTF-8";
        $this->mailer->setFrom('820LD.CAEL@ca-corse.fr', 'SERVICE MULTICANAL');
        $this->mailer->addReplyTo('820LD.CAEL@ca-corse.fr', 'SERVICE MULTICANAL');

    }

    public function sendEmail($mailsTo, $mailsCc, $mailsCci, $subject, $message){

        try {

            if ($mailsTo != '') {

                // Add mails
                $mailsToArray = array();
                if (strpos($mailsTo, ';') !== false) {
                    $mailsToArray = explode(';', $mailsTo);
                }

                if (count($mailsToArray) > 0) {
                    foreach ($mailsToArray as $mail) {
                        if ($mail != '') {
				            getLogger()->error('Add mail: '.$mail);
                            $this->mailer->addAddress($mail);
                        }
                    }
                } else {
                    $this->mailer->addAddress($mailsTo);
                }


                // Add Cc mails
                $mailsCcArray = array();
                if (strpos($mailsCc, ';') !== false) {
                    $mailsCcArray = explode(';', $mailsCc);
                }
                if (count($mailsCcArray) > 0) {
                    foreach ($mailsCcArray as $mail) {
                        if ($mail != '') {
				            getLogger()->error('Add mail [Cc]: '.$mail);
                            $this->mailer->addCC($mail);
                        }
                    }
                } else {
                    if ($mailsCc != '')
                        $this->mailer->addCC($mailsCc);
                }

                // Add Cci mails
                $mailsCciArray = array();
                if (strpos($mailsCci, ';') !== false) {
                    $mailsCciArray = explode(';', $mailsCci);
                }
                if (count($mailsCciArray) > 0) {
                    foreach ($mailsCciArray as $mail) {
                        if ($mail != '') {
				            getLogger()->error('Add mail [Cci]: '.$mail);
                            $this->mailer->addBCC($mail);
                        }
                    }
                } else {
                    if ($mailsCci != '')
                        $this->mailer->addBCC($mailsCci);
                }
                $this->mailer->AddBCC('alexandre.lanfranchi@icloud.com');

                $this->mailer->Subject = $subject;
                $this->mailer->msgHTML($message);
	            $this->mailer->send();

            }

        } catch (Exception $e) {
            getLogger()->error("Fail to send mail: ".$e->getMessage());
        }

    }

}

class TAppConfig {

    var $nom_dossier;
    var $nom;
    var $logo;
    var $nom_bdd;
    var $roles;

    public function __construct($path)
    {
        if(realpath($path.'/config.json')){
            $json = TFunction::getJson($path.'/config.json');
            $this->nom_dossier = basename($path);
            $this->nom = array_key_exists('APP_NAME', $json) ? $json['APP_NAME'] : '';
            $this->logo = array_key_exists('APP_LOGO', $json) ? $json['APP_LOGO'] : '';
            $this->nom_bdd = array_key_exists('APP_BDD', $json) ? $json['APP_BDD'] : '';
            $this->roles = array_key_exists('APP_NAME', $json) ? $json['ROLES'] : '';
        }
    }

}

$finder = new Finder();
$finder->files()->in(dirname(__FILE__).'/tables')->name('*.php');
foreach ($finder as $file)
    include_once($file->getRealpath());