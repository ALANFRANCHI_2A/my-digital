<?php

use Symfony\Component\Finder\Finder;

class T_PROFIL extends ObjectModel{

    protected $definition = array(
        'table' => 'PROFILS',
        'identifier' => 'id',
        'fields' => array(
            'nom' => array('type' => self::TYPE_STRING),
            'id_APPLICATIONS' => array('type' => self::TYPE_INT)
        )
    );

    var $id;
    var $nom;
    var $id_APPLICATIONS;

    public function __construct($id = null){
        parent::__construct($id);
    }

}