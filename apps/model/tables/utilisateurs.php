<?php

use Symfony\Component\HttpFoundation\Request;

class T_UTILISATEUR extends ObjectModel{

    const CREATE = 0;
    const ACTIVE = 1;
    const INACTIVE = -1;

    protected $definition = array(
        'table' => 'UTILISATEURS',
        'identifier' => 'id',
        'fields' => array(
            'guid' => array('type' => self::TYPE_STRING),
            'nom' => array('type' => self::TYPE_STRING),
            'prenom' => array('type' => self::TYPE_STRING),
            'email' => array('type' => self::TYPE_STRING),
            'mdp' => array('type' => self::TYPE_STRING),
            'statut' => array('type' => self::TYPE_INT)
        )
    );

    var $id;
    var $guid;
    var $nom;
    var $prenom;
    var $email;
    var $mdp;
    var $statut;

    public function __construct($id = null){
        parent::__construct($id);
        $this->statut = self::CREATE;
    }

    public function insert($dbHandle = null)
    {
        $this->guid = TFunction::GUID();
        parent::insert($dbHandle);
    }

    public function getRolesUsers($columnResult = false){
        $result = array();
        $apps = new T_APPLICATION();
        $apps = $apps->getAllData();
        $roles = TDatabase::getDataAssoc("`ROLES`,`UTILISATEURS`,`UTILISATEURS_ROLES`","`UTILISATEURS`.`id` = {$this->getId()} AND `UTILISATEURS`.`id` = `UTILISATEURS_ROLES`.`id_UTILISATEURS` AND `ROLES`.`id` = `UTILISATEURS_ROLES`.`id_ROLES` AND `ROLES`.`tag_local_exist` = 1", "`ROLES`.*");
        foreach ($apps as $app){
            $rolesApp = array_filter($roles, function ($elem) use($app) { return $elem['id_APPLICATIONS'] == $app->id; });
            $result[$app->nom_dossier] = ($columnResult) ? array_column($rolesApp, $columnResult) : array_values($rolesApp);
        }
        return $result;
    }

    public function hasRole($roles_name, $folder = false){
        $req = Request::createFromGlobals();
        $result = true;
        $path = $req->getBaseUrl();
        $pathInfo = pathinfo($path);
        $appFolder = ($folder) ? $folder : (($pathInfo['extension'] === NULL) ? basename($path) : basename(dirname($path)));
        $roles = $this->getRolesUsers('tag');
        $roles_name = explode(';', $roles_name);
        foreach ($roles_name as $role_name){
            if(!in_array($role_name, $roles[$appFolder]))
                $result = false;
        }
        return $result;
    }

}