<?php

use Symfony\Component\Finder\Finder;

class T_APPLICATION extends ObjectModel{

    const ACTIVE = 1;
    const INACTIVE = -1;

    const TAG_EXIST = 1;
    const TAG_NOT_EXIST = 0;

    protected $definition = array(
        'table' => 'APPLICATIONS',
        'identifier' => 'id',
        'fields' => array(
            'nom_dossier' => array('type' => self::TYPE_STRING),
            'nom' => array('type' => self::TYPE_STRING),
            'logo' => array('type' => self::TYPE_STRING),
            'nom_bdd' => array('type' => self::TYPE_STRING),
            'statut' => array('type' => self::TYPE_STRING)
        )
    );

    var $id;
    var $nom_dossier;
    var $nom;
    var $logo;
    var $nom_bdd;
    var $statut;

    public function __construct($id = null){
        parent::__construct($id);
        $this->statut = self::INACTIVE;
    }

    public static function setAppsLocalInBDD(){
        $apps = $appsBDD = array();
        $apps[] = new TAppConfig(realpath(dirname(__FILE__).'/../..'));
        $finder = new Finder();
        $finder->files()->in(dirname(__FILE__).'/../../modules')->name('config.json');
        foreach ($finder as $file)
            $apps[] = new TAppConfig($file->getPath());
        foreach ($apps as $app){
            $appBDD = new T_APPLICATION();
            $appBDD->getFromDbByProperty('nom_dossier', $app->nom_dossier);
            $appBDD->nom_dossier = $app->nom_dossier;
            $appBDD->nom = $app->nom;
            $appBDD->logo = $app->logo;
            $appBDD->nom_bdd = $app->nom_bdd;
            $appBDD->save();
            $appsBDD[] = array_merge($appBDD->__toArray(), array('source' => 'LOCAL_AND_BDD'));
        }
        return $appsBDD;
    }

    public function setAppsRolesLocalInBDD(){
        $rolesBDD = array();
        $app = ($this->nom_dossier == 'apps') ? new TAppConfig(realpath(dirname(__FILE__).'/../..')) : new TAppConfig(realpath(dirname(__FILE__).'/../../modules/'.$this->nom_dossier));
        foreach ($app->roles as $key => $value){
            $roleBDD = new T_ROLE();
            $roleBDD = $roleBDD->getAllData("tag = '{$key}' AND id_APPLICATIONS = {$this->getId()}");
            $roleBDD = (count($roleBDD) === 1) ? $roleBDD[0] : new T_ROLE();
            $roleBDD->tag = $key;
            $roleBDD->descr = $value;
            $roleBDD->id_APPLICATIONS = $this->getId();
            $roleBDD->tag_local_exist = self::TAG_EXIST;
            $roleBDD->save();
            $rolesBDD[] = $roleBDD->__toArray();
        }
        $rolesTagNotExist = "'".implode("','",array_column($rolesBDD, 'tag'))."'";
        TDatabase::excuteSqlQuery("UPDATE `ROLES` SET `tag_local_exist` = ". self::TAG_NOT_EXIST ." WHERE `ROLES`.`id_APPLICATIONS` = {$this->getId()} AND `ROLES`.`tag` NOT IN({$rolesTagNotExist})");
    }

}