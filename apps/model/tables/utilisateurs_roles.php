<?php

class T_UTILISATEUR_ROLE extends ObjectModel{

    protected $definition = array(
        'table' => 'UTILISATEURS_ROLES',
        'identifier' => 'id_ROLES',
        'fields' => array(
            'id_UTILISATEURS' => array('type' => self::TYPE_INT)
        )
    );

    var $id_ROLES;
    var $id_UTILISATEURS;

    public function __construct($id = null){
        parent::__construct($id);
    }

}