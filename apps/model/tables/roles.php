<?php

use Symfony\Component\Finder\Finder;

class T_ROLE extends ObjectModel{

    protected $definition = array(
        'table' => 'ROLES',
        'identifier' => 'id',
        'fields' => array(
            'tag' => array('type' => self::TYPE_STRING),
            'descr' => array('type' => self::TYPE_STRING),
            'tag_local_exist' => array('type' => self::TYPE_BOOL),
            'id_APPLICATIONS' => array('type' => self::TYPE_INT)
        )
    );

    var $id;
    var $tag;
    var $descr;
    var $tag_local_exist;
    var $id_APPLICATIONS;

    public function __construct($id = null){
        parent::__construct($id);
    }

}