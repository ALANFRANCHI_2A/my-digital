<?php

use Symfony\Component\Finder\Finder;

class T_ROLE_PROFIL extends ObjectModel{

    protected $definition = array(
        'table' => 'ROLES_PROFILS',
        'identifier' => 'id_PROFILS',
        'fields' => array(
            'id_ROLES' => array('type' => self::TYPE_INT)
        )
    );

    var $id_PROFILS;
    var $id_ROLES;

    public function __construct($id = null){
        parent::__construct($id);
    }

}