<?php

use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;

$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__).'/../views'));

$twig = new Twig_Environment($loader, array('cache' => false));

$finder = new Finder();
$finder->files()->in(realpath(dirname(__FILE__).'/../views'))->name('macro_*.twig');
foreach ($finder as $file)
    $twig->addGlobal(str_replace('.twig', '', $file->getFilename()), $twig->loadTemplate($file->getFilename()));

define('URL_PREFIX', explode('apps', Request::createFromGlobals()->getBasePath())[0].'apps');
$twig->addGlobal('URL_PREFIX', explode('apps', Request::createFromGlobals()->getBasePath())[0].'apps');
$twig->addGlobal('TFunction', new TFunction());

?>