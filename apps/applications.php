<?php

use Symfony\Component\Finder\Finder;

include_once(realpath(dirname(__FILE__).'/model/datamodel.php'));

$session = TApplication::requireAuth();
$user_logged = TApplication::getUserLogged();

$appsLocal = T_APPLICATION::setAppsLocalInBDD();
$appsLocalName = "'".implode("','",array_column($appsLocal, 'nom_dossier'))."'";

$appsNotLocal = new T_APPLICATION();
$appsNotLocal = $appsNotLocal->getAllData('nom_dossier NOT IN(' . $appsLocalName.')');
foreach ($appsNotLocal as $key => $value){
    $appsNotLocal[$key] = $value->__toArray();
    $appsNotLocal[$key]['source'] = 'NOT_LOCAL';
}

$template = $twig->loadTemplate('applications.twig');
$params = array(
    'user_logged' => $user_logged,
    'apps' => array_merge($appsNotLocal, $appsLocal),
);
echo $template->render($params);