<?php

include_once(realpath(dirname(__FILE__).'/model/datamodel.php'));

$session = TApplication::requireAuth();
$user_logged = TApplication::getUserLogged();

$template = $twig->loadTemplate('utilisateurs.twig');
$params = array(
    'user_logged' => $user_logged
);
echo $template->render($params);