<?php

include_once(realpath(dirname(__FILE__).'/model/datamodel.php'));

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

$session = TApplication::requireAuth();
$user_logged = TApplication::getUserLogged();

$template = $twig->loadTemplate('digitickets_acceuil.twig');
$params = array(
    'user_logged' => $user_logged
);
echo $template->render($params);
