<?php

use Symfony\Component\Finder\Finder;

$loader->addPath(realpath(dirname(__FILE__).'/../views'));

$finder = new Finder();
$finder->files()->in(realpath(dirname(__FILE__).'/../views'))->name('macro_*.twig');
foreach ($finder as $file)
    $twig->addGlobal(str_replace('.twig', '', $file->getFilename()), $twig->loadTemplate($file->getFilename()));

?>