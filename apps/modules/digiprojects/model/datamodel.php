<?php

use Symfony\Component\Finder\Finder;

include_once(realpath(dirname(__FILE__).'/../../../model/datamodel.php'));
include_once(realpath(dirname(__FILE__).'/../inc/twiginit.php'));

$finder = new Finder();
$finder->files()->in(dirname(__FILE__).'/tables')->name('*.php');
foreach ($finder as $file)
    include_once($file->getRealpath());