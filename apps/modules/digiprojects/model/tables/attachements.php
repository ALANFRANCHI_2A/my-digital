<?php

use Symfony\Component\Finder\Finder;

class T_ATTACHEMENT extends ObjectModel{

    protected $definition = array(
        'table' => 'ATTACHEMENTS',
        'identifier' => 'id_ATTACHEMENTS',
        'fields' => array(
            'chemin' => array('type' => self::TYPE_STRING),
            'nom' => array('type' => self::TYPE_STRING),
            'date_upload' => array('type' => self::TYPE_DATE),
            'user_id' => array('type' => self::TYPE_INT),
            'id_CAMPAGNES' => array('type' => self::TYPE_INT),
            'id_ACTIONS' => array('type' => self::TYPE_INT),
            'id_CIBLES' => array('type' => self::TYPE_INT),
            'id_COMMENTAIRES' => array('type' => self::TYPE_INT)
        )
    );

    var $id_ATTACHEMENTS;
    var $chemin;
    var $nom;
    var $date_upload;
    var $user_id;
    var $id_CAMPAGNES;
    var $id_ACTIONS;
    var $id_CIBLES;
    var $id_COMMENTAIRES;

    public function __construct($id = null){
        parent::__construct($id);
    }

}