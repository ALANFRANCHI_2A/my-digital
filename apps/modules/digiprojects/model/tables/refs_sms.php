<?php

use Symfony\Component\Finder\Finder;

class T_REF_SMS extends ObjectModel{

    protected $definition = array(
        'table' => 'REFS_SMS',
        'identifier' => 'id_CIBLES',
        'fields' => array(
            'id_TYPES_COMMUNICATIONS' => array('type' => self::TYPE_INT),
            'ref' => array('type' => self::TYPE_STRING)
        )
    );

    var $id_CIBLES;
    var $id_TYPES_COMMUNICATIONS;
    var $ref;

    public function __construct($id = null){
        parent::__construct($id);
        $this->ref = null;
    }

    public function update($dbHandle = null)
    {

        $result = false;
        try {
            $dbHnd = ($dbHandle === null) ? parent::getDatabaseConnect() : $dbHandle;
            if ($dbHnd != null) {

                $fields = $this->definition['fields'];
                $fieldsCount = count($fields);
                $identifier = $this->definition["identifier"];
                $fieldsValues = ($fieldsCount > 0) ? "$identifier = :$identifier, " : "$identifier = :$identifier";

                $i = 0;
                foreach ($fields as $field => $options) {
                    $fieldsValues .= "$field = :$field";
                    if ($i < $fieldsCount - 1) $fieldsValues .= ', ';
                    $i++;
                }

                $sqlReq = 'UPDATE '.$this->getTableName().' SET '.$fieldsValues.' WHERE ('.$identifier.' = :'.$identifier.' AND id_TYPES_COMMUNICATIONS = :id_TYPES_COMMUNICATIONS)';
                getLogger(LOG_FILE_SQL)->info("[{$this->getClassName()}::update(".$this->getId().",".$this->id_TYPES_COMMUNICATIONS.")] SQL-REQ: [$sqlReq]");

                $stmt = $dbHnd->prepare($sqlReq);
                if ($stmt->execute($this->__toArray($this)))
                    $result = true;

            }
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            getLogger()->error("[{$this->getClassName()}::update(".$this->getId().",".$this->id_TYPES_COMMUNICATIONS.")] SQL-ERR: [{$e->getMessage()}] -- SQL-REQ: [$sqlReq]");
        }

        if (!$result)
            return false;

        return $result;

    }

}