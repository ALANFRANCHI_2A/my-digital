<?php

use Symfony\Component\Finder\Finder;

class T_TYPE_COMMUNICATION_CIBLE extends ObjectModel{

    protected $definition = array(
        'table' => 'TYPES_COMMUNICATIONS_CIBLES',
        'identifier' => 'id_CIBLES',
        'fields' => array(
            'id_TYPES_COMMUNICATIONS' => array('type' => self::TYPE_INT)
        )
    );

    var $id_CIBLES;
    var $id_TYPES_COMMUNICATIONS;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function getCible(){
        $cible = new T_CIBLE();
        $cible->getFromDbById($this->id_CIBLES);
        return $cible;
    }

    public function insert($dbHandle = null)
    {
        $histoDescr = array();

        $ref_type_communication_cible = new T_REF_TYPE_COMMUNICATION_CIBLE();
        $ref_type_communication_cible = $ref_type_communication_cible->getAllData("id_CIBLES = {$this->id_CIBLES} AND id_TYPES_COMMUNICATIONS = {$this->id_TYPES_COMMUNICATIONS}");
        if(count($ref_type_communication_cible) === 0){
            $ref_communication = new T_REF_TYPE_COMMUNICATION_CIBLE();
            $ref_communication->id_CIBLES = $this->id_CIBLES;
            $ref_communication->id_TYPES_COMMUNICATIONS = $this->id_TYPES_COMMUNICATIONS;
            $ref_communication->insert();
        }

        $statuts = new T_STATUT();
        $statuts = $statuts->getAllData();
        $cible = $this->getCible();
        $actions = new T_ACTION();
        $actions = $actions->getAllData('id_TYPES_COMMUNICATIONS = ' . $this->id_TYPES_COMMUNICATIONS);
        $currentDate = new DateTime();
        $statutID = array_search('NON DÉMARRÉ', array_column($statuts, 'libelle'));
        foreach ($actions as $action){
            $services = $action->getServices();
            foreach ($services as $service){
                $statutCible = new T_STATUT_TYPE_COMMUNICATION_CIBLE_CAMPAGNE();
                $statutCible->id_CAMPAGNES = $cible->id_CAMPAGNES;
                $statutCible->id_SERVICES = $service->id_SERVICES;
                $statutCible->id_CIBLES = $this->id_CIBLES;
                $statutCible->id_TYPES_COMMUNICATIONS = $this->id_TYPES_COMMUNICATIONS;
                if($statutID !== FALSE){
                    $statutCible->id_STATUTS = intval($statuts[$statutID]->id_STATUTS);
                    $statutCible->user_id = intval(TApplication::getUserLogged()['user']->getId());
                    $statutCible->date = $currentDate->format('Y-m-d H:i:s');
                    $statutCible->insert();
                }
                $actions_service_campagne = new T_ACTION_SERVICE_CAMPAGNE();
                $actions_service_campagne = $actions_service_campagne->getAllData("id_ACTIONS = {$action->getId()} AND id_SERVICES = {$service->id_SERVICES} AND id_CAMPAGNES = {$cible->id_CAMPAGNES}");
                if(count($actions_service_campagne) === 0){
                    $action_service_campagne = new T_ACTION_SERVICE_CAMPAGNE();
                    $action_service_campagne->id_ACTIONS = $action->getId();
                    $action_service_campagne->id_SERVICES = $service->id_SERVICES;
                    $action_service_campagne->id_CAMPAGNES = $cible->id_CAMPAGNES;
                    $action_service_campagne->insert();
                    $statut = new T_STATUT_ACTION_SERVICE_CAMPAGNE();
                    $statut->id_CAMPAGNES = $cible->id_CAMPAGNES;
                    $statut->id_SERVICES = $service->id_SERVICES;
                    $statut->id_ACTIONS = $action->getId();
                    $statutID = array_search('NON DÉMARRÉ', array_column($statuts, 'libelle'));
                    if($statutID !== FALSE){
                        $statut->id_STATUTS = intval($statuts[$statutID]->id_STATUTS);
                        $statut->user_id = intval(TApplication::getUserLogged()['user']->getId());
                        $statut->date = $currentDate->format('Y-m-d H:i:s');
                        $statut->insert();
                        $histoDescr[] = "Ajout de l'action <b>{$action->libelle}</b> pour le service <b>{$service->getServicesDetail()->libelle}</b>";
                    }
                }
            }
        }
        parent::insert($dbHandle);
        return $histoDescr;
    }

}