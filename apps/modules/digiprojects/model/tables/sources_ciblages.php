<?php

use Symfony\Component\Finder\Finder;

class T_SOURCE_CIBLAGE extends ObjectModel{

    protected $definition = array(
        'table' => 'SOURCES_CIBLAGES',
        'identifier' => 'id_SOURCES_CIBLAGES',
        'fields' => array(
            'libelle' => array('type' => self::TYPE_STRING)
        )
    );

    var $id_SOURCES_CIBLAGES;
    var $libelle;

    public function __construct($id = null){
        parent::__construct($id);
    }

}