<?php

use Symfony\Component\Finder\Finder;

class T_HISTORIQUE extends ObjectModel{

    protected $definition = array(
        'table' => 'HISTORIQUES',
        'identifier' => 'id_HISTORIQUES',
        'fields' => array(
            'descr' => array('type' => self::TYPE_STRING),
            'date_historique' => array('type' => self::TYPE_DATE),
            'user_id' => array('type' => self::TYPE_INT),
            'id_CAMPAGNES' => array('type' => self::TYPE_INT),
            'id_ACTIONS' => array('type' => self::TYPE_INT),
            'id_CIBLES' => array('type' => self::TYPE_INT)
        )
    );

    var $id_HISTORIQUE;
    var $descr;
    var $date_historique;
    var $user_id;
    var $id_CAMPAGNES;
    var $id_ACTIONS;
    var $id_CIBLES;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function insert($dbHandle = null)
    {
        $date = new DateTime();
        $this->date_historique = $date->format('Y-m-d H:i:s');
        $this->user_id = TApplication::getUserLogged()['user']->getId();
        return parent::insert($dbHandle);
    }

}