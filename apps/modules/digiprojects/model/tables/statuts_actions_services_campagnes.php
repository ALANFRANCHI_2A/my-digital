<?php

use Symfony\Component\Finder\Finder;

class T_STATUT_ACTION_SERVICE_CAMPAGNE extends ObjectModel{

    protected $definition = array(
        'table' => 'STATUTS_ACTIONS_SERVICES_CAMPAGNES',
        'identifier' => 'id_STATUTS',
        'fields' => array(
            'id_CAMPAGNES' => array('type' => self::TYPE_INT),
            'id_SERVICES' => array('type' => self::TYPE_INT),
            'id_ACTIONS' => array('type' => self::TYPE_INT),
            'date' => array('type' => self::TYPE_DATE),
            'user_id' => array('type' => self::TYPE_INT),
            'commentaire' => array('type' => self::TYPE_STRING)
        )
    );

    var $id_STATUTS;
    var $id_CAMPAGNES;
    var $id_SERVICES;
    var $id_ACTIONS;
    var $date;
    var $user_id;
    var $commentaire;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function getUser(){
        $user = new T_UTILISATEUR();
        $user->getFromDbById($this->user_id);
        return $user;
    }

    public function getStatut(){
        $statut = new T_STATUT();
        $statut->getFromDbById($this->id_STATUTS);
        return $statut;
    }

}