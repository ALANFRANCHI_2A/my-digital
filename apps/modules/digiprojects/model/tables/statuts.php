<?php

use Symfony\Component\Finder\Finder;

class T_STATUT extends ObjectModel{

    protected $definition = array(
        'table' => 'STATUTS',
        'identifier' => 'id_STATUTS',
        'fields' => array(
            'libelle' => array('type' => self::TYPE_STRING),
            'color' => array('type' => self::TYPE_STRING)
        )
    );

    var $id_STATUTS;
    var $libelle;
    var $color;

    public function __construct($id = null){
        parent::__construct($id);
    }

}