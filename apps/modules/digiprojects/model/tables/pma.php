<?php

use Symfony\Component\Finder\Finder;

class T_PMA extends ObjectModel{

    protected $definition = array(
        'table' => 'PMA',
        'identifier' => 'id_PMA',
        'fields' => array(
            'libelle' => array('type' => self::TYPE_STRING)
        )
    );

    var $id_PMA;
    var $libelle;

    public function __construct($id = null){
        parent::__construct($id);
    }

}