<?php

use Symfony\Component\Finder\Finder;

class T_CIBLE extends ObjectModel{

    protected $definition = array(
        'table' => 'CIBLES',
        'identifier' => 'id_CIBLES',
        'fields' => array(
            'num' => array('type' => self::TYPE_INT),
            'detail' => array('type' => self::TYPE_STRING),
            'id_CAMPAGNES' => array('type' => self::TYPE_INT)
        )
    );

    var $id_CIBLES;
    var $num;
    var $detail;
    var $id_CAMPAGNES;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function getNum(){
        $result = TDatabase::getData($this->definition['table'], 'MAX(num) as num', "id_CAMPAGNES = {$this->id_CAMPAGNES}", array(), array(), PDO::FETCH_ASSOC, TDatabase::connect('digiprojects_'));
        return $result[0]['num'];
    }

}