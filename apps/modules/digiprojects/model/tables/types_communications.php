<?php

use Symfony\Component\Finder\Finder;

class T_TYPE_COMMUNICATION extends ObjectModel{

    protected $definition = array(
        'table' => 'TYPES_COMMUNICATIONS',
        'identifier' => 'id_TYPES_COMMUNICATIONS',
        'fields' => array(
            'libelle' => array('type' => self::TYPE_STRING)
        )
    );

    var $id_TYPES_COMMUNICATIONS;
    var $libelle;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function save($dbHandle = null)
    {
        parent::save($dbHandle);
        return $this->updateViewCibles();
    }

    public function updateViewCibles(){

        $db = "digiprojects_";

        $sql = "";
        $sql = "DROP VIEW IF EXISTS {$db}.V_CIBLES;\n";
        $sql .= "CREATE VIEW {$db}.V_CIBLES AS\n";
        $sql .= "SELECT *,\n";

        $types_communications = $this->getAllData();
        foreach($types_communications as $type_communication){
            $id_TYPES_COMMUNICATIONS = intval($type_communication->getId());
            $sql .= "(SELECT COUNT(*)\n";
            $sql .= "FROM {$db}.TYPES_COMMUNICATIONS_CIBLES\n";
            $sql .= "WHERE {$db}.TYPES_COMMUNICATIONS_CIBLES.id_CIBLES = {$db}.CIBLES.id_CIBLES\n";
            $sql .= "AND {$db}.TYPES_COMMUNICATIONS_CIBLES.id_TYPES_COMMUNICATIONS = {$id_TYPES_COMMUNICATIONS}\n";
            $sql .= ") AS type_communication{$id_TYPES_COMMUNICATIONS},\n";
        }

        $sql = substr($sql, 0, -2);

        $sql .= "\nFROM {$db}.CIBLES;";

        return TDatabase::excuteSqlQuery($sql);

    }

}