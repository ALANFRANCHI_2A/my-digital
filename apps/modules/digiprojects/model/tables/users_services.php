<?php

use Symfony\Component\Finder\Finder;

class T_USER_SERVICE extends ObjectModel{

    protected $definition = array(
        'table' => 'USERS_SERVICES',
        'identifier' => 'id_USERS',
        'fields' => array(
            'id_SERVICES' => array('type' => self::TYPE_INT)
        )
    );

    var $id_USERS;
    var $id_SERVICES;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function getUser(){
        $user = new T_UTILISATEUR();
        $user->getFromDbById($this->id_USERS);
        return $user;
    }

}