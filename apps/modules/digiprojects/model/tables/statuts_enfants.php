<?php

use Symfony\Component\Finder\Finder;

class T_STATUT_ENFANTS extends ObjectModel{

    protected $definition = array(
        'table' => 'STATUTS_ENFANTS',
        'identifier' => 'id_STATUTS',
        'fields' => array(
            'id_STATUTS_ENFANTS' => array('type' => self::TYPE_STRING)
        )
    );

    var $id_STATUTS;
    var $id_STATUTS_ENFANTS;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function getStatutEnfant(){
        $statut = new T_STATUT();
        $statut->getFromDbById($this->id_STATUTS_ENFANTS);
        return $statut;
    }

}