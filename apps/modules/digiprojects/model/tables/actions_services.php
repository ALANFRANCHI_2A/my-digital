<?php

use Symfony\Component\Finder\Finder;

class T_ACTION_SERVICE extends ObjectModel{

    protected $definition = array(
        'table' => 'ACTIONS_SERVICES',
        'identifier' => 'id_SERVICES',
        'fields' => array(
            'id_ACTIONS' => array('type' => self::TYPE_INT)
        )
    );

    var $id_SERVICES;
    var $id_ACTIONS;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function getServicesDetail(){
        $service = new T_SERVICE();
        $service->getFromDbById($this->id_SERVICES);
        return $service;
    }

}