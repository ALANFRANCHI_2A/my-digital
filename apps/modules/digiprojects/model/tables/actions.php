<?php

use Symfony\Component\Finder\Finder;

class T_ACTION extends ObjectModel{

    protected $definition = array(
        'table' => 'ACTIONS',
        'identifier' => 'id_ACTIONS',
        'fields' => array(
            'libelle' => array('type' => self::TYPE_STRING),
            'id_TYPES_COMMUNICATIONS' => array('type' => self::TYPE_INT)
        )
    );

    var $id_ACTIONS;
    var $libelle;
    var $id_TYPES_COMMUNICATIONS;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function getTypeCommunication(){
        $type_communication = null;
        if($this->id_TYPES_COMMUNICATIONS !== NULL){
            $type_communication = new T_TYPE_COMMUNICATION();
            $type_communication->getFromDbById($this->id_TYPES_COMMUNICATIONS);
        }
        return $type_communication;
    }

    public function getServices(){
        $action_services = new T_ACTION_SERVICE();
        return $action_services->getAllData("id_ACTIONS = {$this->getId()}");
    }

}