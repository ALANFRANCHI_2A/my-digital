<?php

use Symfony\Component\Finder\Finder;

class T_SERVICE extends ObjectModel{

    protected $definition = array(
        'table' => 'SERVICES',
        'identifier' => 'id_SERVICES',
        'fields' => array(
            'libelle' => array('type' => self::TYPE_STRING)
        )
    );

    var $id_SERVICES;
    var $libelle;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function save($dbHandle = null)
    {
        parent::save($dbHandle);
        return $this->updateViewActionsServices();
    }

    public function updateViewActionsServices(){

        $db = "digiprojects_";

        $sql = "";
        $sql = "DROP VIEW IF EXISTS {$db}.V_ACTIONS_CAMPAGNES;\n";
        $sql .= "CREATE VIEW {$db}.V_ACTIONS_CAMPAGNES AS\n";
        $sql .= "SELECT DISTINCT {$db}.ACTIONS.id_ACTIONS, {$db}.STATUTS_ACTIONS_SERVICES_CAMPAGNES.id_CAMPAGNES, {$db}.ACTIONS.libelle AS action,\n";

        $services = $this->getAllData();
        foreach($services as $service){
            $id_SERVICES = intval($service->getId());
            $sql .= "(SELECT {$db}.V_ACTIONS_SERVICES.statut\n";
            $sql .= "FROM {$db}.V_ACTIONS_SERVICES\n";
            $sql .= "WHERE {$db}.STATUTS_ACTIONS_SERVICES_CAMPAGNES.id_CAMPAGNES = {$db}.V_ACTIONS_SERVICES.id_CAMPAGNES AND {$db}.ACTIONS.id_ACTIONS = {$db}.V_ACTIONS_SERVICES.id_ACTIONS AND {$db}.V_ACTIONS_SERVICES.id_SERVICES = {$id_SERVICES}) AS statut{$id_SERVICES},\n";
            $sql .= "(SELECT {$db}.V_ACTIONS_SERVICES.statut_color\n";
            $sql .= "FROM {$db}.V_ACTIONS_SERVICES\n";
            $sql .= "WHERE {$db}.STATUTS_ACTIONS_SERVICES_CAMPAGNES.id_CAMPAGNES = {$db}.V_ACTIONS_SERVICES.id_CAMPAGNES AND {$db}.ACTIONS.id_ACTIONS = {$db}.V_ACTIONS_SERVICES.id_ACTIONS AND {$db}.V_ACTIONS_SERVICES.id_SERVICES = {$id_SERVICES}) AS color{$id_SERVICES},\n";
        }

        $sql = substr($sql, 0, -2);

        $sql .= "\nFROM {$db}.ACTIONS, {$db}.STATUTS_ACTIONS_SERVICES_CAMPAGNES\n";
        $sql .= "WHERE {$db}.ACTIONS.id_ACTIONS = {$db}.STATUTS_ACTIONS_SERVICES_CAMPAGNES.id_ACTIONS;";

        return TDatabase::excuteSqlQuery($sql);

    }

    public function getUsers(){
        $users_service = new T_USER_SERVICE();
        return $users_service->getAllData("id_SERVICES = {$this->id_SERVICES}");
    }

}