<?php

use Symfony\Component\Finder\Finder;

class T_ACTION_SERVICE_CAMPAGNE extends ObjectModel{

    protected $definition = array(
        'table' => 'ACTIONS_SERVICES_CAMPAGNES',
        'identifier' => 'id_ACTIONS',
        'fields' => array(
            'id_SERVICES' => array('type' => self::TYPE_INT),
            'id_CAMPAGNES' => array('type' => self::TYPE_INT)
        )
    );

    var $id_SERVICES;
    var $id_ACTIONS;
    var $id_CAMPAGNES;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function getActionDetail(){
        $action = new T_ACTION();
        $action->getFromDbById($this->id_ACTIONS);
        return $action;
    }

    public function getService(){
        $service = new T_SERVICE();
        $service->getFromDbById($this->id_SERVICES);
        return $service;
    }

}