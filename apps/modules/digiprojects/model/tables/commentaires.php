<?php

use Symfony\Component\Finder\Finder;

class T_COMMENTAIRE extends ObjectModel{

    protected $definition = array(
        'table' => 'COMMENTAIRES',
        'identifier' => 'id_COMMENTAIRES',
        'fields' => array(
            'contenu' => array('type' => self::TYPE_STRING),
            'date_commentaire' => array('type' => self::TYPE_DATE),
            'user_id' => array('type' => self::TYPE_INT),
            'id_CAMPAGNES' => array('type' => self::TYPE_INT),
            'id_ACTIONS' => array('type' => self::TYPE_INT),
            'id_CIBLES' => array('type' => self::TYPE_INT),
            'id_COMMENTAIRES_PARENT' => array('type' => self::TYPE_INT)
        )
    );

    var $id_COMMENTAIRES;
    var $contenu;
    var $date_commentaire;
    var $user_id;
    var $id_CAMPAGNES;
    var $id_ACTIONS;
    var $id_CIBLES;
    var $id_COMMENTAIRES_PARENT;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function getAttachements(){
        $attachements = new T_ATTACHEMENT();
        return $attachements->getAllData("id_COMMENTAIRES = {$this->id_COMMENTAIRES}");
    }

}