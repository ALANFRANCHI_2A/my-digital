<?php

use Symfony\Component\Finder\Finder;

class T_CAMPAGNE extends ObjectModel{

    protected $definition = array(
        'table' => 'CAMPAGNES',
        'identifier' => 'id_CAMPAGNES',
        'fields' => array(
            'nom' => array('type' => self::TYPE_STRING),
            'id_PMA' => array('type' => self::TYPE_INT),
            'date_commercialisation' => array('type' => self::TYPE_DATE),
            'date_debut' => array('type' => self::TYPE_DATE),
            'date_fin' => array('type' => self::TYPE_DATE),
            'recurrent' => array('type' => self::TYPE_BOOL),
            'user_id' => array('type' => self::TYPE_INT),
            'id_UNIVERS_BESOINS' => array('type' => self::TYPE_INT)
        )
    );

    var $id_CAMPAGNES;
    var $nom;
    var $id_PMA;
    var $date_commercialisation;
    var $date_debut;
    var $date_fin;
    var $recurrent;
    var $user_id;
    var $id_UNIVERS_BESOINS;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function insert($dbHandle = null)
    {
        parent::insert($dbHandle);
        $histo = new T_HISTORIQUE();
        $histo->descr = 'Ajout de la campagne';
        $histo->id_CAMPAGNES = $this->getId();
        $histo->id_ACTIONS = null;
        $histo->id_CIBLES = null;
        return $histo->insert();
    }

}