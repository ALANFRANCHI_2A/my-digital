<?php

use Symfony\Component\Finder\Finder;

class T_REF_TYPE_COMMUNICATION_CIBLE extends ObjectModel{

    protected $definition = array(
        'table' => 'REFS_TYPES_COMMUNICATIONS_CIBLES',
        'identifier' => 'id_CIBLES',
        'fields' => array(
            'id_TYPES_COMMUNICATIONS' => array('type' => self::TYPE_INT),
            'date_envoi' => array('type' => self::TYPE_DATE),
            'volumetrie' => array('type' => self::TYPE_STRING)
        )
    );

    var $id_CIBLES;
    var $id_TYPES_COMMUNICATIONS;
    var $date_envoi;
    var $volumetrie;

    public function __construct($id = null){
        parent::__construct($id);
    }

    public function insert($dbHandle = null)
    {
        $ref_comm_detail = false;
        switch (intval($this->id_TYPES_COMMUNICATIONS)){
            case 1:
                $ref_comm_detail = new T_REF_EMAILING();
                break;
            case 2:
                $ref_comm_detail = new T_REF_EMESSAGE();
                break;
            case 3:
                $ref_comm_detail = new T_REF_NPC();
                break;
            case 4:
                $ref_comm_detail = new T_REF_SMS();
                break;
            case 5:
                $ref_comm_detail = new T_REF_OPC();
                break;
        }
        $ref_comm_detail->id_CIBLES = $this->id_CIBLES;
        $ref_comm_detail->id_TYPES_COMMUNICATIONS = $this->id_TYPES_COMMUNICATIONS;
        $ref_comm_detail->insert();
        return parent::insert($dbHandle);
    }

    public function update($dbHandle = null)
    {

        $result = false;
        try {
            $dbHnd = ($dbHandle === null) ? parent::getDatabaseConnect() : $dbHandle;
            if ($dbHnd != null) {

                $fields = $this->definition['fields'];
                $fieldsCount = count($fields);
                $identifier = $this->definition["identifier"];
                $fieldsValues = ($fieldsCount > 0) ? "$identifier = :$identifier, " : "$identifier = :$identifier";

                $i = 0;
                foreach ($fields as $field => $options) {
                    $fieldsValues .= "$field = :$field";
                    if ($i < $fieldsCount - 1) $fieldsValues .= ', ';
                    $i++;
                }

                $sqlReq = 'UPDATE '.$this->getTableName().' SET '.$fieldsValues.' WHERE ('.$identifier.' = :'.$identifier.' AND id_TYPES_COMMUNICATIONS = :id_TYPES_COMMUNICATIONS)';
                getLogger(LOG_FILE_SQL)->info("[{$this->getClassName()}::update(".$this->getId().",".$this->id_TYPES_COMMUNICATIONS.")] SQL-REQ: [$sqlReq]");

                $stmt = $dbHnd->prepare($sqlReq);
                if ($stmt->execute($this->__toArray($this)))
                    $result = true;

            }
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            getLogger()->error("[{$this->getClassName()}::update(".$this->getId().",".$this->id_TYPES_COMMUNICATIONS.")] SQL-ERR: [{$e->getMessage()}] -- SQL-REQ: [$sqlReq]");
        }

        if (!$result)
            return false;

        return $result;

    }

    public function getCibleDetail(){
        $cible = new T_CIBLE();
        $cible->getFromDbById($this->id_CIBLES);
        return $cible;
    }

    public function getRefDetail(){
        $ref = false;
        switch (intval($this->id_TYPES_COMMUNICATIONS)){
            case 1:
                $ref = new T_REF_EMAILING();
                break;
            case 2:
                $ref = new T_REF_EMESSAGE();
                break;
            case 3:
                $ref = new T_REF_NPC();
                break;
            case 4:
                $ref = new T_REF_SMS();
                break;
            case 5:
                $ref = new T_REF_OPC();
                break;
        }
        $ref = $ref->getAllData("id_CIBLES = {$this->id_CIBLES} AND id_TYPES_COMMUNICATIONS = {$this->id_TYPES_COMMUNICATIONS}");
        return (count($ref) > 0) ? $ref[0] : false;
    }

}