<?php

use Symfony\Component\Finder\Finder;

class T_POID_OPC extends ObjectModel{

    protected $definition = array(
        'table' => 'POIDS_OPC',
        'identifier' => 'id_POIDS_OPC',
        'fields' => array(
            'libelle' => array('type' => self::TYPE_STRING)
        )
    );

    var $id_POIDS_OPC;
    var $libelle;

    public function __construct($id = null){
        parent::__construct($id);
    }

}