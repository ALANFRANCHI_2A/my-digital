<?php

use Symfony\Component\Finder\Finder;

class T_UNIVERS_BESOIN extends ObjectModel{

    protected $definition = array(
        'table' => 'UNIVERS_BESOINS',
        'identifier' => 'id_UNIVERS_BESOINS',
        'fields' => array(
            'libelle' => array('type' => self::TYPE_STRING)
        )
    );

    var $id_UNIVERS_BESOINS;
    var $libelle;

    public function __construct($id = null){
        parent::__construct($id);
    }

}