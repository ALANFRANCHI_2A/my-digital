<?php

include_once(realpath(dirname(__FILE__).'/model/datamodel.php'));

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

$session = TApplication::requireAuth();
$user_logged = TApplication::getUserLogged();

if(!$user_logged['user']->hasRole('ACCESS')){
    $response = new RedirectResponse(TApplication::getUrlPrefix());
    $response->send();
    exit(0);
}

$services = new T_SERVICE();

$template = $twig->loadTemplate('digiprojets_acceuil.twig');
$params = array(
    'user_logged' => $user_logged,
    'services' => $services->getAllData()
);
echo $template->render($params);