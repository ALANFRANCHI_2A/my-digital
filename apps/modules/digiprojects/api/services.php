<?php

include_once(realpath(dirname(__FILE__).'/../../../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

$app->get(API_URL_PREFIX.'/services/list', function() use ($app){

    $services = new T_SERVICE();
    $services = $services->getAllData();

    echo json_encode(array(
        'services' => $services
    ));
    exit;

});
$app->get(API_URL_PREFIX.'/services/{id_CAMPAGNES}/{id_ACTIONS}', function($id_CAMPAGNES, $id_ACTIONS) use ($app){

    $services = new T_SERVICE();
    $services = $services->getAllData();

    $services_action_statuts = new T_STATUT_ACTION_SERVICE_CAMPAGNE();
    $services_action_statuts = $services_action_statuts->getAllData("id_CAMPAGNES = {$id_CAMPAGNES} AND id_ACTIONS = {$id_ACTIONS}");

    $services_action = [];
    foreach ($services as $service){
        if(in_array($service->getId(), array_column($services_action_statuts, 'id_SERVICES')))
            $services_action[] = $service;
    }

    $user_id = intval(TApplication::getUserLogged()['user']->getId());
    $user_service = new T_USER_SERVICE();
    $user_service = $user_service->getAllData("id_USERS = {$user_id}");
    $user_service = (count($user_service) > 0) ? $user_service[0] : new T_USER_SERVICE();

    $action = new T_ACTION();
    $action->getFromDbById($id_ACTIONS);

    echo json_encode(array(
        'services' => $services_action,
        'user_service' => $user_service,
        'action' => $action
    ));
    exit;

});