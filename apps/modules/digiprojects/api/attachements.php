<?php

include_once(realpath(dirname(__FILE__).'/../../../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

define('BASE_PATH', realpath(realpath(dirname(__FILE__).'/../../../')));
define('DATA_UPLOAD_RELATIVE_PATH', realpath(dirname(__FILE__).'/../../../data/digiprojects_/files_upload/'));

$app->post(API_URL_PREFIX.'/attachements/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'id_ATTACHEMENTS,chemin,nom,date_upload,id_CAMPAGNES,id_ACTIONS,id_CIBLES,nom_complet';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult('digiprojects_');

    echo json_encode($result->read('V_ATTACHEMENTS', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});

$app->post(API_URL_PREFIX.'/attachements/{id_CAMPAGNES}/{id_ACTIONS}/{id_CIBLES}', function($id_CAMPAGNES,$id_ACTIONS,$id_CIBLES) use ($app){

    $req = Request::createFromGlobals();
    $result = false;
    $attach = new T_ATTACHEMENT();

    $id_CAMPAGNES = (intval($id_CAMPAGNES) > 0) ? intval($id_CAMPAGNES) : null;
    $id_ACTIONS = (intval($id_ACTIONS) > 0) ? intval($id_ACTIONS) : null;
    $id_CIBLES = (intval($id_CIBLES) > 0) ? intval($id_CIBLES) : null;
    $id_COMMENTAIRES = null;
    $user_id = intval(TApplication::getUserLogged()['user']->getId());

    $name = $req->request->get('name');

    if ($req->files->has("file")  && $req->files->get("file") !== NULL) {
        if ($req->files->get("file")->getError() == 0) {
            $file = $req->files->get("file");
            $filename = TFunction::getUniqueTime().'_'.$file->getClientOriginalName();
            try {
                $file->move(DATA_UPLOAD_RELATIVE_PATH, $filename);
                $fullpath = str_replace(BASE_PATH, '', realpath(DATA_UPLOAD_RELATIVE_PATH.'/'.$filename));
                $attach->nom = $name;
                $attach->chemin = $fullpath;
                $attach->user_id = $user_id;
                $currentDate = new DateTime();
                $attach->date_upload = $currentDate->format('Y-m-d H:i:s');
                $attach->id_CAMPAGNES = $id_CAMPAGNES;
                $attach->id_ACTIONS = $id_ACTIONS;
                $attach->id_COMMENTAIRES = $id_COMMENTAIRES;
                $attach->id_CIBLES = $id_CIBLES;
                $result = $attach->insert();
            } catch (Exception $e) {
                //TODO: log a message and send back this same form
                exit();
            }
        }
    }

    echo json_encode(array(
        'result' => $result,
        'attach' => $attach
    ));
    exit;

});

$app->delete(API_URL_PREFIX.'/attachement/{id_ATTACHEMENTS}', function($id_ATTACHEMENTS) use ($app){

    $req = Request::createFromGlobals();
    $result = false;

    $attach = new T_ATTACHEMENT();
    $attach->getFromDbById($id_ATTACHEMENTS);

    if($attach->getId() > 0){
        unlink(BASE_PATH.$attach->chemin);
        $result = $attach->delete();
    }

    echo json_encode(array(
        'result' => $result
    ));
    exit;

});