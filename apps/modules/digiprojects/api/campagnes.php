<?php

include_once(realpath(dirname(__FILE__).'/../../../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

function formatDateBDD($dateFr){
    $date = DateTime::createFromFormat('d/m/Y', $dateFr);
    return ($date) ? $date->format('Y-m-d') : null;
}
function formatDateFr($dateEn){
    $date = DateTime::createFromFormat('Y-m-d', $dateEn);
    return ($date) ? $date->format('d/m/Y') : null;
}

$app->post(API_URL_PREFIX.'/campagnes/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'id_CAMPAGNES,nom,univers,pma,date_commercialisation,date_debut,date_fin,recurrent,nom_complet,nb_actions,nb_non_demarrer,nb_en_cours,nb_terminer,nb_abandonner,nb_retard';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult('digiprojects_');

    echo json_encode($result->read('V_CAMPAGNES', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->get(API_URL_PREFIX.'/campagnes/{id_CAMPAGNES}/popup', function($id_CAMPAGNES) use ($app, $twig){

    $html = "";

    $campagne = new T_CAMPAGNE();
    $campagne->getFromDbById($id_CAMPAGNES);

    $univers = new T_UNIVERS_BESOIN();
    $univers = $univers->getAllData();

    $users = TDatabase::getDataAssoc('apps_gestion.UTILISATEURS');

    $pma = new T_PMA();
    $pma = $pma->getAllData();

    $template = $twig->loadTemplate('digiprojets_block-campagne-popup.twig');
    $params = array(
        'campagne' => $campagne,
        'univers' => $univers,
        'users' => $users,
        'pma' => $pma
    );
    $html .= $template->render($params);

    if($campagne->getId() > 0) {

        $services = new T_SERVICE();
        $services = $services->getAllData();

        $actions = new T_ACTION();
        $actions = $actions->getAllData('id_TYPES_COMMUNICATIONS IS NULL');

        $actions_services = new T_ACTION_SERVICE();
        $actions_services = $actions_services->getAllData();

        $actions_services_campagnes = new T_ACTION_SERVICE_CAMPAGNE();
        $actions_services_campagnes = $actions_services_campagnes->getAllData("id_CAMPAGNES = {$campagne->getId()}");

        foreach ($services as $service) {

            //Récupération des lignes qui sont égal à id_SERVICES
            $actionsService = array_filter($actions_services, function ($e) use ($service) {
                return $e->id_SERVICES === $service->id_SERVICES;
            });

            //Transformation du tableau d'objet en un tableau avec les id_ACTIONS uniquement
            $actionsService = array_column($actionsService, 'id_ACTIONS');

            //Transformation des chaines de caractère en entiers
            $actionsService = array_map(function ($a) {
                return intval($a);
            }, $actionsService);

            $actionsResult = array_filter($actions, function ($e) use ($actionsService) {
                return in_array(intval($e->id_ACTIONS), $actionsService) !== FALSE;
            });

            foreach ($actionsResult as $key => $action) {
                $result = array_filter($actions_services_campagnes, function ($e) use ($service, $action) {
                    return intval($e->id_SERVICES) === intval($service->id_SERVICES) && intval($e->id_ACTIONS) === intval($action->id_ACTIONS);
                });
                $actionsResult[$key]->checked = (count($result) === 1) ? true : false;
            }

            $template = $twig->loadTemplate('digiprojets_block-actions-popup.twig');
            $params = array(
                'service' => $service,
                'actions' => $actionsResult
            );

            $html .= $template->render($params);

        }

    }

    echo json_encode(array(
        'html' => $html,
        'campagne' => $campagne
    ));
    exit;

});
$app->post(API_URL_PREFIX.'/campagnes/add', function() use ($app, $twig){

    $req = Request::createFromGlobals();
    $result = false;
    $errors = array();

    $nom = $req->request->has('nom') ? $req->request->get('nom') : '';
    $id_PMA = $req->request->has('id_PMA') ? $req->request->get('id_PMA') : '';
    $date_commercialisation = $req->request->has('date_commercialisation') ? $req->request->get('date_commercialisation') : null;
    $date_debut = $req->request->has('date_debut') ? $req->request->get('date_debut') : null;
    $date_fin = $req->request->has('date_fin') ? $req->request->get('date_fin') : null;
    $id_UNIVERS_BESOINS = $req->request->has('id_UNIVERS_BESOINS') ? intval($req->request->get('id_UNIVERS_BESOINS')) : 0;
    $univers_libelle = $req->request->has('univers_libelle') ? $req->request->get('univers_libelle') : '';
    $user_id = $req->request->has('user_id') ? $req->request->get('user_id') : null;
    $recurrent = $req->request->has('recurrent') ? $req->request->get('recurrent') : null;

    if($nom === "")
        $errors[] = 'Nom requis';
    if($id_PMA === "")
        $errors[] = 'PMA requis';
    if($date_commercialisation === "")
        $errors[] = 'Date de commercialisation requis';
    if($date_debut === "")
        $errors[] = 'Date de début des actions requis';
    //if($date_fin === "")
        //$errors[] = 'Date de fin des actions requis';
    if($user_id === "")
        $errors[] = 'Chargé requis';

    if($id_UNIVERS_BESOINS === 0 && $univers_libelle === "")
        $errors[] = 'Nom de l\'univers de besoin requis';

    if($id_UNIVERS_BESOINS === 0 && $univers_libelle !== ""){
        $univers_besoin = new T_UNIVERS_BESOIN();
        $univers_besoin->libelle = $univers_libelle;
        $univers_besoin->insert();
        $id_UNIVERS_BESOINS = $univers_besoin->getId();
    }

    if(count($errors) === 0){

        $campagne = new T_CAMPAGNE();
        $campagne->nom = $nom;
        $campagne->id_PMA = $id_PMA;
        $campagne->date_commercialisation = formatDateBDD($date_commercialisation);
        $campagne->date_debut = formatDateBDD($date_debut);
        $campagne->date_fin = ($date_fin === "") ? null : formatDateBDD($date_fin);
        $campagne->recurrent = $recurrent;
        $campagne->id_UNIVERS_BESOINS = $id_UNIVERS_BESOINS;
        $campagne->user_id = $user_id;

        if($campagne->insert()){
            $result = true;
            $currentUser = TApplication::getUserLogged()['user'];
            $template = $twig->loadTemplate('digiprojets_email.twig');
            //$twig->addGlobal('URL_PREFIX_DIGIPROJECTS', 'http://localhost:8888/SITES/PHC/apps');
            $twig->addGlobal('URL_PREFIX_DIGIPROJECTS', 'http://mydigital.ca-corse.fr/apps');
            $mail = new TMailManager();
            $emails = TDatabase::getDataAssoc("digiprojects_.USERS_SERVICES AS USERS_SERVICES, apps_gestion.UTILISATEURS", "USERS_SERVICES.id_USERS = UTILISATEURS.id", "DISTINCT UTILISATEURS.email");
            $mail->sendEmail(implode(";", array_column($emails, 'email')),'','','[DIGIPROJECTS] : Nouvelle campagne',$template->render(array(
                'user' => false,
                'contenu' => "<b>{$currentUser->prenom} {$currentUser->nom}</b> vient de créer la campagne &laquo;&nbsp;<b>{$campagne->nom}</b>&nbsp;&raquo;"
            )));
        }

    }

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors,
        'campagne' => $campagne
    ));
    exit;

});
$app->get(API_URL_PREFIX.'/campagnes/{id}/infos', function($id) use ($app, $twig){

    $html = "";

    $campagne = TDatabase::getDataAssoc('digiprojects_.V_CAMPAGNES', "id_CAMPAGNES = {$id}");
    $campagne = (count($campagne) === 1) ? $campagne[0] : new T_CAMPAGNE();

    $template = $twig->loadTemplate('digiprojets_block-campagne_infos.twig');
    $params = array(
        'campagne' => $campagne
    );
    $html .= $template->render($params);

    echo json_encode(array(
        'html' => $html,
    ));
    exit;

});
$app->post(API_URL_PREFIX.'/campagnes/edit', function() use ($app, $twig){

    $req = Request::createFromGlobals();
    $histoDescr = array();
    $notificationsEmail = array();
    $result = false;
    $errors = array();

    $id_CAMPAGNES = $req->request->has('id_CAMPAGNES') ? intval($req->request->get('id_CAMPAGNES')) : 0;
    $nom = $req->request->has('nom') ? $req->request->get('nom') : '';
    $id_PMA = $req->request->has('id_PMA') ? $req->request->get('id_PMA') : '';
    $date_commercialisation = $req->request->has('date_commercialisation') ? $req->request->get('date_commercialisation') : null;
    $date_debut = $req->request->has('date_debut') ? $req->request->get('date_debut') : null;
    $date_fin = $req->request->has('date_fin') ? $req->request->get('date_fin') : null;
    $id_UNIVERS_BESOINS = $req->request->has('id_UNIVERS_BESOINS') ? intval($req->request->get('id_UNIVERS_BESOINS')) : 0;
    $univers_libelle = $req->request->has('univers_libelle') ? $req->request->get('univers_libelle') : '';
    $user_id = $req->request->has('user_id') ? $req->request->get('user_id') : null;
    $recurrent = $req->request->has('recurrent') ? $req->request->get('recurrent') : null;

    $actions = $req->request->has('actions') ? $req->request->get('actions') : [];

    $statuts = new T_STATUT();
    $statuts = $statuts->getAllData();

    $actionsBDD = new T_ACTION();
    $actionsBDD = $actionsBDD->getAllData();
    $servicesBDD = new T_SERVICE();
    $servicesBDD = $servicesBDD->getAllData();
    $universBDD = new T_UNIVERS_BESOIN();
    $universBDD = $universBDD->getAllData();
    $usersBDD = TDatabase::getDataAssoc('apps_gestion.UTILISATEURS');

    foreach ($actions as $action){
        $action = explode(';',$action);
        $service = intval($action[0]);
        $action = intval($action[1]);
        $actionDB = new T_ACTION_SERVICE_CAMPAGNE();
        $actionDB = $actionDB->getAllData("id_CAMPAGNES = {$id_CAMPAGNES} AND id_SERVICES = {$service} AND id_ACTIONS = {$action}");
        if(count($actionDB) === 0){
            $actionBDD_ID = array_search($action, array_column($actionsBDD, 'id_ACTIONS'));
            $serviceBDD_ID = array_search($service, array_column($servicesBDD, 'id_SERVICES'));
            $actionDB = new T_ACTION_SERVICE_CAMPAGNE();
            $actionDB->id_CAMPAGNES = $id_CAMPAGNES;
            $actionDB->id_SERVICES = $service;
            $actionDB->id_ACTIONS = $action;
            $actionDB->insert();
            $statut = new T_STATUT_ACTION_SERVICE_CAMPAGNE();
            $statut->id_CAMPAGNES = $id_CAMPAGNES;
            $statut->id_SERVICES = $service;
            $statut->id_ACTIONS = $action;
            $statutID = array_search('NON DÉMARRÉ', array_column($statuts, 'libelle'));
            if($statutID !== FALSE){
                $statut->id_STATUTS = intval($statuts[$statutID]->id_STATUTS);
                $statut->user_id = intval(TApplication::getUserLogged()['user']->getId());
                $currentDate = new DateTime();
                $statut->date = $currentDate->format('Y-m-d H:i:s');
                $statut->insert();
            }
            $histoDescr[] = "Ajout de l'action <b>{$actionsBDD[$actionBDD_ID]->libelle}</b> pour le service <b>{$servicesBDD[$serviceBDD_ID]->libelle}</b>";
            $notificationsEmail[$servicesBDD[$serviceBDD_ID]->id_SERVICES]['add'][] = $actionsBDD[$actionBDD_ID]->libelle;
        }
    }

    $actionsDB = new T_ACTION_SERVICE_CAMPAGNE();
    $actionsDB = $actionsDB->getAllData("id_CAMPAGNES = {$id_CAMPAGNES}");

    foreach ($actionsDB as $action){
        $actionDetail = $action->getActionDetail();
        if($actionDetail->id_TYPES_COMMUNICATIONS === NULL){
            $actionStr = $action->id_SERVICES.';'.$action->id_ACTIONS;
            if(!in_array($actionStr,$actions)){
                $actionBDD_ID = array_search($action->id_ACTIONS, array_column($actionsBDD, 'id_ACTIONS'));
                $serviceBDD_ID = array_search($action->id_SERVICES, array_column($servicesBDD, 'id_SERVICES'));
                TDatabase::excuteSqlQuery("DELETE FROM `ACTIONS_SERVICES_CAMPAGNES` WHERE `ACTIONS_SERVICES_CAMPAGNES`.`id_ACTIONS` = {$action->id_ACTIONS} AND `ACTIONS_SERVICES_CAMPAGNES`.`id_SERVICES` = {$action->id_SERVICES} AND `ACTIONS_SERVICES_CAMPAGNES`.`id_CAMPAGNES` = {$action->id_CAMPAGNES}", TDatabase::connect('digiprojects_'));
                //$action->delete();
                $statut = new T_STATUT_ACTION_SERVICE_CAMPAGNE();
                $statut->id_CAMPAGNES = $id_CAMPAGNES;
                $statut->id_SERVICES = $action->id_SERVICES;
                $statut->id_ACTIONS = $action->id_ACTIONS;
                $statutID = array_search('ABANDONNÉ', array_column($statuts, 'libelle'));
                if($statutID !== FALSE){
                    $statut->id_STATUTS = intval($statuts[$statutID]->id_STATUTS);
                    $statut->user_id = intval(TApplication::getUserLogged()['user']->getId());
                    $currentDate = new DateTime();
                    $statut->date = $currentDate->format('Y-m-d H:i:s');
                    $statut->insert();
                }
                $histoDescr[] = "Suppression de l'action <b>{$actionsBDD[$actionBDD_ID]->libelle}</b> pour le service <b>{$servicesBDD[$serviceBDD_ID]->libelle}</b>";
                $notificationsEmail[$servicesBDD[$serviceBDD_ID]->id_SERVICES]['delete'][] = $actionsBDD[$actionBDD_ID]->libelle;
            }
        }
    }

    $currentUser = TApplication::getUserLogged()['user'];
    $template = $twig->loadTemplate('digiprojets_email.twig');
    //$twig->addGlobal('URL_PREFIX_DIGIPROJECTS', 'http://localhost:8888/SITES/PHC/apps');
    $twig->addGlobal('URL_PREFIX_DIGIPROJECTS', 'http://mydigital.ca-corse.fr/apps');

    foreach ($notificationsEmail as $serviceID => $notif){
        $contenu = "<b>{$currentUser->prenom} {$currentUser->nom}</b> vient de mettre à jour les actions de la campagne &laquo;&nbsp;<b>{$nom}</b>&nbsp;&raquo;<br><br>";
        if(count($notif['add']) > 0){
            $contenu .= "Voici les actions ajoutées pour votre services :<ul>";
            foreach ($notif['add'] as $actionAdd)
                $contenu .= "<li>$actionAdd</li>";
            $contenu .= "</ul><br>";
        }
        if(count($notif['delete']) > 0){
            $contenu .= "Voici les actions supprimées pour votre services :<ul>";
            foreach ($notif['delete'] as $actionDelete)
                $contenu .= "<li>$actionDelete</li>";
            $contenu .= "</ul>";
        }
        $mail = new TMailManager();
        $emails = TDatabase::getDataAssoc("digiprojects_.USERS_SERVICES AS USERS_SERVICES, apps_gestion.UTILISATEURS", "USERS_SERVICES.id_SERVICES = $serviceID AND USERS_SERVICES.id_USERS = UTILISATEURS.id", "DISTINCT UTILISATEURS.email");
        $mail->sendEmail(implode(";", array_column($emails, 'email')),'','',"[DIGIPROJECTS] : Modification de la campagne $nom",$template->render(array(
            'user' => false,
            'contenu' => $contenu
        )));
    }

    if($nom === "")
        $errors[] = 'Nom requis';
    if($id_PMA === "")
        $errors[] = 'PMA requis';
    if($date_commercialisation === "")
        $errors[] = 'Date de commercialisation requis';
    if($date_debut === "")
        $errors[] = 'Date de début des actions requis';
    //if($date_fin === "")
        //$errors[] = 'Date de fin des actions requis';
    if($user_id === "")
        $errors[] = 'Chargé requis';

    if($id_UNIVERS_BESOINS === 0 && $univers_libelle === "")
        $errors[] = 'Nom de l\'univers de besoin requis';

    if($id_UNIVERS_BESOINS === 0 && $univers_libelle !== ""){
        $univers_besoin = new T_UNIVERS_BESOIN();
        $univers_besoin->libelle = $univers_libelle;
        $univers_besoin->insert();
        $id_UNIVERS_BESOINS = $univers_besoin->getId();
    }

    if(count($errors) === 0){
        $campagne = new T_CAMPAGNE();
        $campagne->getFromDbById($id_CAMPAGNES);
        if($campagne->nom != $nom)
            $histoDescr[] = "Changement du nom de la campagne ({$campagne->nom} => {$nom})";
        $campagne->nom = $nom;
        if($campagne->id_PMA != $id_PMA){
            $oldPMA = new T_PMA();
            $oldPMA->getFromDbById($campagne->id_PMA);
            $newPMA = new T_PMA();
            $newPMA->getFromDbById($id_PMA);
            $histoDescr[] = "Changement du PMA de la campagne ({$oldPMA->libelle} => {$newPMA->libelle})";
        }
        $campagne->id_PMA = $id_PMA;
        if($campagne->date_commercialisation != formatDateBDD($date_commercialisation))
            $histoDescr[] = "Changement de la date de commercialisation de la campagne (". formatDateFr($campagne->date_commercialisation) ." => {$date_commercialisation})";
        $campagne->date_commercialisation = formatDateBDD($date_commercialisation);
        if($campagne->date_debut != formatDateBDD($date_debut))
            $histoDescr[] = "Changement de la date de début des actions de la campagne (". formatDateFr($campagne->date_debut) . " => {$date_debut})";
        $campagne->date_debut = formatDateBDD($date_debut);
        if($campagne->date_fin != $date_fin){
            if($campagne->date_fin == null)
                $histoDescr[] = "Ajout de la date de fin des actions de la campagne ({$date_fin})";
            elseif($date_fin == null)
                $histoDescr[] = "Suppression de la date de fin des actions de la campagne (". formatDateFr($campagne->date_fin) .")";
            else
                $histoDescr[] = "Changement de la date de fin des actions de la campagne (". formatDateFr($campagne->date_fin) ." => {$date_fin})";
        }
        $campagne->date_fin = ($date_fin === "") ? null : formatDateBDD($date_fin);
        if($campagne->recurrent != $recurrent){
            $libelleOldRecurrent = ($campagne->recurrent == 0) ? 'NON' : 'OUI';
            $libelleNewRecurrent = ($recurrent == 0) ? 'NON' : 'OUI';
            $histoDescr[] = "Changement du paramètre récurrent de la campagne ({$libelleOldRecurrent} => {$libelleNewRecurrent})";
        }
        $campagne->id_PMA = $id_PMA;
        if($campagne->id_UNIVERS_BESOINS != $id_UNIVERS_BESOINS){
            $univerBDD_NewID = array_search($id_UNIVERS_BESOINS, array_column($universBDD, 'id_UNIVERS_BESOINS'));
            $univerBDD_OldID = array_search($campagne->id_UNIVERS_BESOINS, array_column($universBDD, 'id_UNIVERS_BESOINS'));
            $histoDescr[] = "Changement de l'univers de besoin de la campagne ({$universBDD[$univerBDD_OldID]->libelle} => {$universBDD[$univerBDD_NewID]->libelle})";
        }
        $campagne->id_UNIVERS_BESOINS = $id_UNIVERS_BESOINS;
        if($campagne->user_id != $user_id){
            $usersBDD_NewID = array_search($user_id, array_column($usersBDD, 'id'));
            $usersBDD_OldID = array_search($campagne->user_id, array_column($usersBDD, 'id'));
            $histoDescr[] = "Changement du chargé de la campagne ({$usersBDD[$usersBDD_OldID]['prenom']} {$usersBDD[$usersBDD_OldID]['nom']} => {$usersBDD[$usersBDD_NewID]['prenom']} {$usersBDD[$usersBDD_NewID]['nom']})";
        }
        $campagne->user_id = $user_id;
        $campagne->recurrent = $recurrent;
        $result = $campagne->update();
        $histo = new T_HISTORIQUE();
        $histo->descr = implode(PHP_EOL, $histoDescr);
        $histo->id_CAMPAGNES = $campagne->getId();
        $histo->id_ACTIONS = null;
        $histo->id_CIBLES = null;
        $histo->insert();
    }

    echo json_encode(array(
        'result' => true,
        'errors' => $errors
    ));
    exit;

});