<?php

include_once(realpath(dirname(__FILE__).'/../../../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

$app->post(API_URL_PREFIX.'/cibles/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = array();

    $types_communications = new T_TYPE_COMMUNICATION();
    $types_communications = $types_communications->getAllData();

    $tab[] = 'id_CIBLES';
    $tab[] = 'num';
    $tab[] = 'detail';
    $tab[] = 'id_CAMPAGNES';

    foreach ($types_communications as $type_communication){
        $tab[] = 'type_communication'.$type_communication->getId();
    }

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult('digiprojects_');

    echo json_encode($result->read('V_CIBLES', $tab, $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->get(API_URL_PREFIX.'/cibles/{id_CIBLES}/popup', function($id_CIBLES) use ($app, $twig){

    $html = "";

    $cible = new T_CIBLE();
    $cible->getFromDbById($id_CIBLES);

    $types_communications = new T_TYPE_COMMUNICATION();
    $types_communications = $types_communications->getAllData();

    if($cible->getId() > 0){
        $types_communications_cible = new T_TYPE_COMMUNICATION_CIBLE();
        $types_communications_cible = $types_communications_cible->getAllData("id_CIBLES = {$cible->getId()}");
        $types_communications_cible = array_column($types_communications_cible, 'id_TYPES_COMMUNICATIONS');
        foreach ($types_communications as $key => $types_communication)
            $types_communications[$key]->checked = (in_array($types_communication->getId(), $types_communications_cible)) ? true : false;
    }

    $template = $twig->loadTemplate('digiprojets_block-cible-popup.twig');
    $params = array(
        'cible' => $cible,
        'types_communications' => $types_communications
    );
    $html .= $template->render($params);

    echo json_encode(array(
        'html' => $html
    ));
    exit;

});
$app->post(API_URL_PREFIX.'/cibles/{id_CAMPAGNES}/save', function($id_CAMPAGNES) use ($app, $twig){

    $req = Request::createFromGlobals();
    $currentUser = TApplication::getUserLogged()['user'];
    $notifications = "";
    $histoDescr = array();
    $result = false;
    $errors = array();

    $campagne = new T_CAMPAGNE();
    $campagne->getFromDbById($id_CAMPAGNES);

    $id_CIBLES = $req->request->has('id_CIBLES') ? intval($req->request->get('id_CIBLES')) : 0;
    $detail = $req->request->has('detail') ? $req->request->get('detail') : '';
    $types_communications_cible = $req->request->has('types_communications_cible') ? array_map('intval',$req->request->get('types_communications_cible')) : [];

    $cible = new T_CIBLE();
    $cible->getFromDbById($id_CIBLES);
    $cible->id_CAMPAGNES = $id_CAMPAGNES;

    if($cible->getId() === 0)
        $cible->num = (intval($cible->getNum()) === 0) ? 1 : (intval($cible->getNum()) + 1);

    if($cible->getId() > 0){
        if($cible->detail !== $detail)
            $histoDescr[] = "Modification du détail de la <b>CIBLE N°{$cible->num}</b>";
        $notifications .= "<b>{$currentUser->prenom} {$currentUser->nom}</b> vient de mettre à jour la cible n°{$cible->num} pour la campagne &laquo;&nbsp;<b>{$campagne->nom}</b>&nbsp;&raquo;<br><br>";
    } else{
        $histoDescr[] = "Ajout de la <b>CIBLE N°{$cible->num}</b>";
        $notifications .= "<b>{$currentUser->prenom} {$currentUser->nom}</b> vient d'ajouter une cible pour la campagne &laquo;&nbsp;<b>{$campagne->nom}</b>&nbsp;&raquo;<br><br>";
    }

    $cible->detail = $detail;
    $cible->save();

    $types_communications_cible_BEFORE = new T_TYPE_COMMUNICATION_CIBLE();
    $types_communications_cible_BEFORE = $types_communications_cible_BEFORE->getAllData("id_CIBLES = {$cible->getId()}");
    $ids_TYPES_COMMUNICATIONS_CIBLE_BEFORE = array_map('intval', array_column($types_communications_cible_BEFORE, 'id_TYPES_COMMUNICATIONS'));

    if(count($types_communications_cible) > 0)
        $notifications .= "Voici les communications ajoutées : <br><ul>";
    foreach ($types_communications_cible as $id_TYPES_COMMUNICATIONS){

        $type_communication = new T_TYPE_COMMUNICATION();
        $type_communication->getFromDbById($id_TYPES_COMMUNICATIONS);

        if(in_array($id_TYPES_COMMUNICATIONS, $ids_TYPES_COMMUNICATIONS_CIBLE_BEFORE)){
            $array_INDEX = array_search($id_TYPES_COMMUNICATIONS, $ids_TYPES_COMMUNICATIONS_CIBLE_BEFORE);
            unset($types_communications_cible_BEFORE[$array_INDEX]);
        }else {
            $type_communication_cible = new T_TYPE_COMMUNICATION_CIBLE();
            $type_communication_cible->id_CIBLES = $cible->getId();
            $type_communication_cible->id_TYPES_COMMUNICATIONS = $id_TYPES_COMMUNICATIONS;
            $histoDescrAction = $type_communication_cible->insert();
            $histoDescr[] = "Ajout <b>{$type_communication->libelle}</b> pour la <b>CIBLE N°{$cible->num}</b>";
            foreach ($histoDescrAction as $histo)
                $histoDescr[] = $histo;
            $notifications .= "<li>{$type_communication->libelle}</li>";
        }

    }
    if(count($types_communications_cible) > 0)
        $notifications .= "</ul>";

    $statuts = new T_STATUT();
    $statuts = $statuts->getAllData();

    $cibles_campagne = new T_CIBLE();
    $cibles_campagne = $cibles_campagne->getAllData("id_CAMPAGNES = {$id_CAMPAGNES}");
    $cibles_campagne = array_column($cibles_campagne, 'id_CIBLES');
    $cibles_campagne = implode("','", $cibles_campagne);

    $types_communications_cible_BEFORE = array_column(array_values($types_communications_cible_BEFORE), 'id_TYPES_COMMUNICATIONS');
    if(count($types_communications_cible_BEFORE) > 0)
        $notifications .= "Voici les communications abandonnées : <br><ul>";
    foreach ($types_communications_cible_BEFORE as $id_TYPES_COMMUNICATIONS){

        $type_communication = new T_TYPE_COMMUNICATION();
        $type_communication->getFromDbById($id_TYPES_COMMUNICATIONS);
        TDatabase::excuteSqlQuery("DELETE FROM TYPES_COMMUNICATIONS_CIBLES WHERE id_CIBLES = {$cible->getId()} AND id_TYPES_COMMUNICATIONS = {$id_TYPES_COMMUNICATIONS}", TDatabase::connect('digiprojects_'));

        $cibles_campagne_types_communication = new T_TYPE_COMMUNICATION_CIBLE();
        $cibles_campagne_types_communication = $cibles_campagne_types_communication->getAllData("id_CIBLES IN ('{$cibles_campagne}') AND id_TYPES_COMMUNICATIONS = {$id_TYPES_COMMUNICATIONS}");

        $actions = new T_ACTION();
        $actions = $actions->getAllData('id_TYPES_COMMUNICATIONS = ' . $id_TYPES_COMMUNICATIONS);
        $statutID = array_search('ABANDONNÉ', array_column($statuts, 'libelle'));
        $currentDate = new DateTime();
        foreach ($actions as $action){
            $services = $action->getServices();
            foreach ($services as $service){
                if($statutID !== FALSE){
                    $statutCible = new T_STATUT_TYPE_COMMUNICATION_CIBLE_CAMPAGNE();
                    $statutCible->id_CAMPAGNES = $cible->id_CAMPAGNES;
                    $statutCible->id_TYPES_COMMUNICATIONS = $action->id_TYPES_COMMUNICATIONS;
                    $statutCible->id_CIBLES = $cible->id_CIBLES;
                    $statutCible->id_SERVICES = $service->id_SERVICES;
                    $statutCible->id_STATUTS = intval($statuts[$statutID]->id_STATUTS);
                    $statutCible->user_id = intval(TApplication::getUserLogged()['user']->getId());
                    $statutCible->date = $currentDate->format('Y-m-d H:i:s');
                    $statutCible->insert();
                    if(count($cibles_campagne_types_communication) === 0){
                        TDatabase::excuteSqlQuery("DELETE FROM digiprojects_.ACTIONS_SERVICES_CAMPAGNES WHERE id_ACTIONS = {$action->getId()} AND id_SERVICES = {$service->id_SERVICES} AND id_CAMPAGNES = {$cible->id_CAMPAGNES}");
                        $statut = new T_STATUT_ACTION_SERVICE_CAMPAGNE();
                        $statut->id_CAMPAGNES = $cible->id_CAMPAGNES;
                        $statut->id_SERVICES = $service->id_SERVICES;
                        $statut->id_ACTIONS = $action->getId();
                        $statut->id_STATUTS = intval($statuts[$statutID]->id_STATUTS);
                        $statut->user_id = intval(TApplication::getUserLogged()['user']->getId());
                        $statut->date = $currentDate->format('Y-m-d H:i:s');
                        $statut->insert();
                    }
                }
            }
            $histoDescr[] = "Suppression <b>{$type_communication->libelle}</b> pour la <b>CIBLE N°{$cible->num}</b>";
            $notifications .= "<li>{$type_communication->libelle}</li>";
        }

    }
    if(count($types_communications_cible_BEFORE) > 0)
        $notifications .= "</ul>";

    if(count($types_communications_cible_BEFORE) > 0 && count($types_communications_cible) === 0)
        $histoDescr[] = "Abandon de la <b>CIBLE N°{$cible->num}</b>";

    if(count($histoDescr) > 0){
        $histo = new T_HISTORIQUE();
        $histo->descr = implode(PHP_EOL, $histoDescr);
        $histo->id_CAMPAGNES = $id_CAMPAGNES;
        $histo->id_ACTIONS = null;
        $histo->id_CIBLES = null;
        $result = $histo->insert();
    }else
        $result = true;

    $template = $twig->loadTemplate('digiprojets_email.twig');
    //$twig->addGlobal('URL_PREFIX_DIGIPROJECTS', 'http://localhost:8888/SITES/PHC/apps');
    $twig->addGlobal('URL_PREFIX_DIGIPROJECTS', 'http://mydigital.ca-corse.fr/apps');
    $mail = new TMailManager();
    $emails = TDatabase::getDataAssoc("digiprojects_.USERS_SERVICES AS USERS_SERVICES, apps_gestion.UTILISATEURS", "USERS_SERVICES.id_USERS = UTILISATEURS.id", "DISTINCT UTILISATEURS.email");
    $mail->sendEmail(implode(";", array_column($emails, 'email')),'','','[DIGIPROJECTS] : Cibles',$template->render(array(
        'user' => false,
        'contenu' => $notifications
    )));

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors
    ));
    exit;

});