<?php

include_once(realpath(dirname(__FILE__).'/../../../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

$app->get(API_URL_PREFIX.'/commentaires/{id_CAMPAGNES}/{id_ACTIONS}/{id_CIBLES}', function($id_CAMPAGNES, $id_ACTIONS, $id_CIBLES) use ($app, $twig){

    $html = "";

    $id_ACTIONS = intval($id_ACTIONS);
    $id_CIBLES = intval($id_CIBLES);
    $criteriaAction = ($id_ACTIONS > 0) ? "= {$id_ACTIONS}" : 'IS NULL';
    $criteriaCible = ($id_CIBLES > 0) ? "= {$id_CIBLES}" : 'IS NULL';

    $commentaires = TDatabase::getDataClass('digiprojects_.V_COMMENTAIRES', '*', "id_CAMPAGNES = {$id_CAMPAGNES} AND id_COMMENTAIRES_PARENT IS NULL AND id_ACTIONS {$criteriaAction} AND id_CIBLES {$criteriaCible}", array(), array(), null, 'T_COMMENTAIRE');

    $template = $twig->loadTemplate('digiprojets_block-commentaire.twig');
    foreach ($commentaires as $key => $comment){

        $params = array(
            'comment' => $comment,
            'user_logged' => TApplication::getUserLogged()
        );
        $html .= $template->render($params);

        $sub_commentaires = TDatabase::getDataClass('digiprojects_.V_COMMENTAIRES', '*', "id_CAMPAGNES = {$id_CAMPAGNES} AND id_COMMENTAIRES_PARENT = {$comment->id_COMMENTAIRES} AND id_ACTIONS {$criteriaAction} AND id_CIBLES {$criteriaCible}", array(), array(), null, 'T_COMMENTAIRE');
        foreach ($sub_commentaires as $sub_comment){
            $params = array(
                'comment' => $sub_comment,
                'user_logged' => TApplication::getUserLogged()
            );
            $html .= $template->render($params);
        }

    }

    echo json_encode(array(
        'html' => $html,
        'commentaires' => $commentaires
    ));
    exit;

});
$app->post(API_URL_PREFIX.'/commentaires/{id_CAMPAGNES}/{id_ACTIONS}/{id_CIBLES}', function($id_CAMPAGNES, $id_ACTIONS, $id_CIBLES) use ($app, $twig){

    $req = Request::createFromGlobals();
    $errors = [];
    $result = false;
    $comment = new T_COMMENTAIRE();

    $id_CAMPAGNES = (intval($id_CAMPAGNES) > 0) ? intval($id_CAMPAGNES) : null;
    $id_ACTIONS = (intval($id_ACTIONS) > 0) ? intval($id_ACTIONS) : null;
    $id_CIBLES = (intval($id_CIBLES) > 0) ? intval($id_CIBLES) : null;
    $id_COMMENTAIRES_PARENT = $req->request->has('id_COMMENTAIRES_PARENT') ? $req->request->get('id_COMMENTAIRES_PARENT') : null;

    $comment->contenu = $req->request->has('comment') ? $req->request->get('comment') : null;
    $attachements = $req->request->has('attachements') ? $req->request->get('attachements') : [];
    $attachements = (empty($attachements)) ? [] : explode(',', $attachements);

    if(empty($comment->contenu) && count($attachements) === 0)
        $errors[] = 'Le commentaire ne peux pas être vide';

    if(count($errors) === 0 && !empty($comment->contenu)){

        $currentDate = new DateTime();
        $comment->date_commentaire = $currentDate->format('Y-m-d H:i:s');
        $comment->user_id = intval(TApplication::getUserLogged()['user']->getId());
        $comment->id_CAMPAGNES = $id_CAMPAGNES;
        $comment->id_ACTIONS = $id_ACTIONS;
        $comment->id_CIBLES = $id_CIBLES;
        $comment->id_COMMENTAIRES_PARENT = (intval($id_COMMENTAIRES_PARENT) > 0) ? intval($id_COMMENTAIRES_PARENT) : null;

        if($comment->id_COMMENTAIRES_PARENT > 0){
            $fakeCommentaire = new T_COMMENTAIRE();
            $fakeCommentaire->getFromDbById($comment->id_COMMENTAIRES_PARENT);
            $comment->id_COMMENTAIRES_PARENT = (intval($fakeCommentaire->id_COMMENTAIRES_PARENT) > 0) ? intval($fakeCommentaire->id_COMMENTAIRES_PARENT) : $fakeCommentaire->getId();
        }

        $result = $comment->insert();

        foreach ($attachements as $attachID){
            $attachID = intval($attachID);
            $attach = new T_ATTACHEMENT();
            $attach->getFromDbById($attachID);
            $attach->id_COMMENTAIRES = $comment->getId();
            $attach->update();
        }

        //Envoi mail
        $currentUser = TApplication::getUserLogged()['user'];

        $campagne = new T_CAMPAGNE();
        $campagne->getFromDbById($id_CAMPAGNES);
        $action = new T_ACTION();
        if($id_ACTIONS > 0)
            $action->getFromDbById($id_ACTIONS);
        $cible = new T_CIBLE();
        if($id_CIBLES > 0)
            $cible->getFromDbById($id_CIBLES);

        $contenu = "<b>{$currentUser->prenom} {$currentUser->nom}</b> vient d'ajouter un nouveau commentaire : <br><br>";
        $contenu .= "<b>CAMPAGNE :</b> " . $campagne->nom ."<br>";
        if($action->getId() > 0)
            $contenu .= "<b>ACTION :</b> " . $action->libelle ."<br>";
        if($cible->getId() > 0)
            $contenu .= "<b>CIBLE :</b> CIBLE " . $cible->num ."<br>";
        $contenu .= "<b>MESSAGE :</b> &laquo;&nbsp;<i>".$comment->contenu."</i>&nbsp;&raquo;";

        $mail = new TMailManager();
        $template = $twig->loadTemplate('digiprojets_email.twig');
        //$twig->addGlobal('URL_PREFIX_DIGIPROJECTS', 'http://localhost:8888/SITES/PHC/apps');
        $twig->addGlobal('URL_PREFIX_DIGIPROJECTS', 'http://mydigital.ca-corse.fr/apps');

        $users_to = $req->request->has('users_to') ? $req->request->get('users_to') : [];
        foreach ($users_to as $user_to){
            $user_to = explode('_', $user_to);
            $serviceID = intval(substr($user_to[0],1));
            $userID = (count($user_to) > 0) ? intval(substr($user_to[1],1)) : 0;
            if($userID > 0){
                $user = new T_UTILISATEUR();
                $user->getFromDbById($userID);
                $params = array(
                    'user' => $user,
                    'contenu' => $contenu
                );
                //$mail->sendEmail($user->email,'','','[DIGIPROJECTS] : Nouveau commentaire',$template->render($params));
            }
        }

    }else if(count($errors) === 0)
        $result = true;

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors
    ));
    exit;

});
