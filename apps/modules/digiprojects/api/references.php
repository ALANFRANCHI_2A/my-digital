<?php

include_once(realpath(dirname(__FILE__).'/../../../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

$app->get(API_URL_PREFIX.'/reference/{id_ACTIONS}/{id_CIBLES}/form', function($id_ACTIONS, $id_CIBLES) use ($app, $twig){

    $html = "";

    $action = new T_ACTION();
    $action->getFromDbById($id_ACTIONS);
    $type_communication = $action->getTypeCommunication();

    $ref = new T_REF_TYPE_COMMUNICATION_CIBLE();
    $ref = $ref->getAllData("id_CIBLES = {$id_CIBLES} AND id_TYPES_COMMUNICATIONS = {$type_communication->id_TYPES_COMMUNICATIONS}");
    $ref = (count($ref) > 0) ? $ref[0] : new T_REF_TYPE_COMMUNICATION_CIBLE();

    $template = $twig->loadTemplate('digiprojets_form-ref-comm.twig');
    $html .= $template->render(array(
        'ref' => $ref,
        'cible' => $ref->getCibleDetail()
    ));

    $template = false;
    $params = [];
    switch (intval($type_communication->id_TYPES_COMMUNICATIONS)){
        case 1:
            $template = $twig->loadTemplate('digiprojets_form-ref-comm-sms.twig');
            break;
        case 2:
            $template = $twig->loadTemplate('digiprojets_form-ref-comm-sms.twig');
            break;
        case 3:
            $codes_projets = TDatabase::getDataAssoc('REFS_NPC', '', 'DISTINCT code_projet', TDatabase::connect('digiprojects_'));
            $params['codes_projets'] = $codes_projets;
            $template = $twig->loadTemplate('digiprojets_form-ref-comm-npc.twig');
            break;
        case 4:
            $template = $twig->loadTemplate('digiprojets_form-ref-comm-sms.twig');
            break;
        case 5:
            $poids_opc = new T_POID_OPC();
            $sources_ciblages = new T_SOURCE_CIBLAGE();
            $params['poids_opc'] = $poids_opc->getAllData();
            $params['sources_ciblages'] = $sources_ciblages->getAllData();
            $template = $twig->loadTemplate('digiprojets_form-ref-comm-opc.twig');
            break;
    }

    if($template){
        $params['ref'] = $ref->getRefDetail();
        $html .= $template->render($params);
    }

    echo json_encode(array(
        'html' => $html,
        'ref' => $ref,
        'ref_detail' => $ref->getRefDetail()
    ));
    exit;

});
$app->post(API_URL_PREFIX.'/reference/save', function() use ($app){


    $req = Request::createFromGlobals();
    $histoDescr = array();
    $result = true;
    $errors = array();

    $id_CIBLES = $req->request->has('id_CIBLES') ? intval($req->request->get('id_CIBLES')) : 0;
    $id_TYPES_COMMUNICATIONS = $req->request->has('id_TYPES_COMMUNICATIONS') ? intval($req->request->get('id_TYPES_COMMUNICATIONS')) : 0;
    $date_envoi = $req->request->has('date_envoi') ? ($req->request->get('date_envoi') == "") ? null : formatDateBDD($req->request->get('date_envoi')) : null;
    $volumetrie = $req->request->has('volumetrie') ? ($req->request->get('volumetrie') == "") ? null : $req->request->get('volumetrie') : null;

    $ref = new T_REF_TYPE_COMMUNICATION_CIBLE();
    $ref->id_CIBLES = $id_CIBLES;
    $ref->id_TYPES_COMMUNICATIONS = $id_TYPES_COMMUNICATIONS;
    $ref->date_envoi = $date_envoi;
    $ref->volumetrie = $volumetrie;
    $result = $ref->update();

    $refDetail = false;
    switch (intval($id_TYPES_COMMUNICATIONS)){
        case 1:
            $refDetail = new T_REF_EMAILING();
            break;
        case 2:
            $refDetail = new T_REF_EMESSAGE();
            break;
        case 3:
            $refDetail = new T_REF_NPC();
            break;
        case 4:
            $refDetail = new T_REF_SMS();
            break;
        case 5:
            $refDetail = new T_REF_OPC();
            break;
    }

    $refDetail->id_CIBLES = $id_CIBLES;
    $refDetail->id_TYPES_COMMUNICATIONS = $id_CIBLES;

    foreach ($refDetail->getFields() as $field){
        if($field['options']['type'] == ObjectModelCore::TYPE_DATE)
            $refDetail->{$field['field']} = $req->request->has($field['field']) ? ($req->request->get($field['field']) == "") ? null : formatDateBDD($req->request->get($field['field'])) : null;
        else
            $refDetail->{$field['field']} = $req->request->has($field['field']) ? ($req->request->get($field['field']) == "") ? null : $req->request->get($field['field']) : null;
    }
    $refDetail->update();

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors
    ));
    exit;

});
