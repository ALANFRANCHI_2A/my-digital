<?php

include_once(realpath(dirname(__FILE__).'/../../../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

$app->get(API_URL_PREFIX.'/statuts/{id_CAMPAGNES}/{id_SERVICES}/{id_ACTIONS}', function($id_CAMPAGNES, $id_SERVICES, $id_ACTIONS) use ($app, $twig){

    $html = $html_select_statut =  "";

    $statuts = new T_STATUT_ACTION_SERVICE_CAMPAGNE();
    $statuts = $statuts->getAllData("id_CAMPAGNES = {$id_CAMPAGNES} AND id_SERVICES = {$id_SERVICES} AND id_ACTIONS = {$id_ACTIONS} ORDER BY date ASC");

    $template = $twig->loadTemplate('digiprojets_block-statut.twig');
    foreach ($statuts as $statut){
        $params = array(
            'statut' => $statut
        );
        $html .= $template->render($params);
    }

    $lastStatut = $statuts[count($statuts)-1];
    $statuts_enfants = new T_STATUT_ENFANTS();
    $statuts_enfants = $statuts_enfants->getAllData("id_STATUTS = {$lastStatut->id_STATUTS}");

    $html_select_statut .= '<option></option>';
    foreach ($statuts_enfants as $statut_enfants){
        $html_select_statut .= '<option value="' . $statut_enfants->getStatutEnfant()->id_STATUTS . '">' . $statut_enfants->getStatutEnfant()->libelle . '</option>';
    }

    echo json_encode(array(
        'html' => $html,
        'html_select_statut' => $html_select_statut
    ));
    exit;

});
$app->get(API_URL_PREFIX.'/statuts/{id_CAMPAGNES}/{id_SERVICES}/{id_ACTIONS}/{id_CIBLES}', function($id_CAMPAGNES, $id_SERVICES, $id_ACTIONS, $id_CIBLES) use ($app, $twig){

    $html = $html_select_statut =  "";

    $action = new T_ACTION();
    $action->getFromDbById($id_ACTIONS);
    $id_TYPES_COMMUNICATIONS = $action->getTypeCommunication()->id_TYPES_COMMUNICATIONS;

    $statuts = new T_STATUT_TYPE_COMMUNICATION_CIBLE_CAMPAGNE();
    $statuts = $statuts->getAllData("id_CAMPAGNES = {$id_CAMPAGNES} AND id_SERVICES = {$id_SERVICES} AND id_TYPES_COMMUNICATIONS = {$id_TYPES_COMMUNICATIONS} AND id_CIBLES = {$id_CIBLES} ORDER BY date ASC");

    $template = $twig->loadTemplate('digiprojets_block-statut.twig');
    foreach ($statuts as $statut){
        $params = array(
            'statut' => $statut
        );
        $html .= $template->render($params);
    }

    $html_select_statut .= '<option></option>';

    if(count($statuts) > 0){
        $lastStatut = $statuts[count($statuts)-1];
        $statuts_enfants = new T_STATUT_ENFANTS();
        $statuts_enfants = $statuts_enfants->getAllData("id_STATUTS = {$lastStatut->id_STATUTS}");

        foreach ($statuts_enfants as $statut_enfants){
            $html_select_statut .= '<option value="' . $statut_enfants->getStatutEnfant()->id_STATUTS . '">' . $statut_enfants->getStatutEnfant()->libelle . '</option>';
        }
    }

    echo json_encode(array(
        'html' => $html,
        'html_select_statut' => $html_select_statut
    ));
    exit;

});

$app->post(API_URL_PREFIX.'/statuts/{id_CAMPAGNES}/{id_SERVICES}/{id_ACTIONS}', function($id_CAMPAGNES, $id_SERVICES, $id_ACTIONS) use ($app, $twig){

    $req = Request::createFromGlobals();
    $errors = [];
    $result = true;

    $id_CAMPAGNES = (intval($id_CAMPAGNES) > 0) ? intval($id_CAMPAGNES) : null;
    $id_SERVICES = (intval($id_SERVICES) > 0) ? intval($id_SERVICES) : null;
    $id_ACTIONS = (intval($id_ACTIONS) > 0) ? intval($id_ACTIONS) : null;

    $newStatut = $req->request->has('statut_service_action') ? $req->request->get('statut_service_action') : null;
    $comment = $req->request->has('comment_statut') ? $req->request->get('comment_statut') : null;

    $statut_action_service = new T_STATUT_ACTION_SERVICE_CAMPAGNE();

    $statut_action_service->id_STATUTS = (intval($newStatut) > 0) ? intval($newStatut) : null;

    $statut_action_service->id_CAMPAGNES = $id_CAMPAGNES;
    $statut_action_service->id_SERVICES = $id_SERVICES;
    $statut_action_service->id_ACTIONS = $id_ACTIONS;

    $currentDate = new DateTime();
    $statut_action_service->date = $currentDate->format('Y-m-d H:i:s');
    $statut_action_service->user_id = intval(TApplication::getUserLogged()['user']->getId());

    $statut_action_service->commentaire = ($comment == "") ? null : $comment;

    if($statut_action_service->id_STATUTS > 0)
        $result = $statut_action_service->insert();

    if($statut_action_service->id_STATUTS == 5){
        $campagne = new T_CAMPAGNE();
        $campagne->getFromDbById($id_CAMPAGNES);
        $action = new T_ACTION();
        $action->getFromDbById($id_ACTIONS);
        $service = new T_SERVICE();
        $service->getFromDbById($id_SERVICES);
        $statut = new T_STATUT();
        $statut->getFromDbById($statut_action_service->id_STATUTS);
        $currentUser = TApplication::getUserLogged()['user'];
        $template = $twig->loadTemplate('digiprojets_email.twig');
        //$twig->addGlobal('URL_PREFIX_DIGIPROJECTS', 'http://localhost:8888/SITES/PHC/apps');
        $twig->addGlobal('URL_PREFIX_DIGIPROJECTS', 'http://mydigital.ca-corse.fr/apps');
        $mail = new TMailManager();
        $emails = TDatabase::getDataAssoc("digiprojects_.USERS_SERVICES AS USERS_SERVICES, apps_gestion.UTILISATEURS", "USERS_SERVICES.id_USERS = UTILISATEURS.id", "DISTINCT UTILISATEURS.email");
        $mail->sendEmail(implode(";", array_column($emails, 'email')),'','','[DIGIPROJECTS] : Changement de statut',$template->render(array(
            'user' => false,
            'contenu' => "<b>{$currentUser->prenom} {$currentUser->nom}</b> vient de passer le statut de l'action <b>{$action->libelle}</b> pour le service <b>{$service->libelle}</b> de la camapgne <b>{$campagne->nom}</b> à <b>{$statut->libelle}</b>"
        )));
    }

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors
    ));
    exit;

});
$app->post(API_URL_PREFIX.'/statuts/{id_CAMPAGNES}/{id_SERVICES}/{id_ACTIONS}/{id_CIBLES}', function($id_CAMPAGNES, $id_SERVICES, $id_ACTIONS, $id_CIBLES) use ($app, $twig){

    $req = Request::createFromGlobals();
    $errors = [];
    $result = true;

    $id_CAMPAGNES = (intval($id_CAMPAGNES) > 0) ? intval($id_CAMPAGNES) : null;
    $id_SERVICES = (intval($id_SERVICES) > 0) ? intval($id_SERVICES) : null;
    $id_ACTIONS = (intval($id_ACTIONS) > 0) ? intval($id_ACTIONS) : null;
    $id_CIBLES = (intval($id_CIBLES) > 0) ? intval($id_CIBLES) : null;

    $action = new T_ACTION();
    $action->getFromDbById($id_ACTIONS);

    $newStatut = $req->request->has('statut_service_action') ? $req->request->get('statut_service_action') : null;
    $comment = $req->request->has('comment_statut') ? $req->request->get('comment_statut') : null;

    $statut_action_service_cible = new T_STATUT_TYPE_COMMUNICATION_CIBLE_CAMPAGNE();

    $statut_action_service_cible->id_STATUTS = (intval($newStatut) > 0) ? intval($newStatut) : null;

    $statut_action_service_cible->id_CAMPAGNES = $id_CAMPAGNES;
    $statut_action_service_cible->id_TYPES_COMMUNICATIONS = $action->getTypeCommunication()->id_TYPES_COMMUNICATIONS;
    $statut_action_service_cible->id_CIBLES = $id_CIBLES;
    $statut_action_service_cible->id_SERVICES = $id_SERVICES;

    $currentDate = new DateTime();
    $statut_action_service_cible->date = $currentDate->format('Y-m-d H:i:s');
    $statut_action_service_cible->user_id = intval(TApplication::getUserLogged()['user']->getId());

    $statut_action_service_cible->commentaire = ($comment == "") ? null : $comment;

    if($statut_action_service_cible->id_STATUTS > 0)
        $result = $statut_action_service_cible->insert();

    if($statut_action_service_cible->id_STATUTS == 5){
        $campagne = new T_CAMPAGNE();
        $campagne->getFromDbById($id_CAMPAGNES);
        $action = new T_ACTION();
        $action->getFromDbById($id_ACTIONS);
        $service = new T_SERVICE();
        $service->getFromDbById($id_SERVICES);
        $cible = new T_CIBLE();
        $cible->getFromDbById($id_CIBLES);
        $statut = new T_STATUT();
        $statut->getFromDbById($statut_action_service_cible->id_STATUTS);
        $currentUser = TApplication::getUserLogged()['user'];
        $template = $twig->loadTemplate('digiprojets_email.twig');
        //$twig->addGlobal('URL_PREFIX_DIGIPROJECTS', 'http://localhost:8888/SITES/PHC/apps');
        $twig->addGlobal('URL_PREFIX_DIGIPROJECTS', 'http://mydigital.ca-corse.fr/apps');
        $mail = new TMailManager();
        $emails = TDatabase::getDataAssoc("digiprojects_.USERS_SERVICES AS USERS_SERVICES, apps_gestion.UTILISATEURS", "USERS_SERVICES.id_USERS = UTILISATEURS.id", "DISTINCT UTILISATEURS.email");
        $mail->sendEmail(implode(";", array_column($emails, 'email')),'','','[DIGIPROJECTS] : Changement de statut',$template->render(array(
            'user' => false,
            'contenu' => "<b>{$currentUser->prenom} {$currentUser->nom}</b> vient de passer le statut de l'action <b>{$action->libelle}</b> pour la cible <b>{$cible->detail}</b> du service <b>{$service->libelle}</b> de la campagne <b>{$campagne->nom}</b> à <b>{$statut->libelle}</b>"
        )));
    }

    // Mise à jours automatique du statut de l'action
    $cibles_campagne = new T_CIBLE();
    $cibles_campagne = $cibles_campagne->getAllData("id_CAMPAGNES = {$id_CAMPAGNES}");
    $cibles_campagne = array_column($cibles_campagne, 'id_CIBLES');
    $cibles_campagne = implode("','", $cibles_campagne);

    $cibles_campagne_action = new T_REF_TYPE_COMMUNICATION_CIBLE();
    $cibles_campagne_action = $cibles_campagne_action->getAllData("id_CIBLES IN ('{$cibles_campagne}') AND id_TYPES_COMMUNICATIONS = {$action->getTypeCommunication()->id_TYPES_COMMUNICATIONS}");

    $statuts = array();
    foreach ($cibles_campagne_action as $cible){
        $statut = new T_STATUT_TYPE_COMMUNICATION_CIBLE_CAMPAGNE();
        $statut = $statut->getAllData("id_CAMPAGNES = {$id_CAMPAGNES} AND id_TYPES_COMMUNICATIONS = {$action->getTypeCommunication()->id_TYPES_COMMUNICATIONS} AND id_CIBLES = {$cible->id_CIBLES} AND id_SERVICES = {$id_SERVICES} ORDER BY date DESC LIMIT 1");
        $statuts[] = intval($statut[0]->id_STATUTS);
    }
    $statutsCount = array_count_values($statuts);

    $statutsBDD = new T_STATUT();
    $statutsBDD = $statutsBDD->getAllData();

    $statutID_NON_DEMARRER = intval($statutsBDD[array_search('NON DÉMARRÉ', array_column($statutsBDD, 'libelle'))]->id_STATUTS);
    $statutID_EN_COURS = intval($statutsBDD[array_search('EN COURS', array_column($statutsBDD, 'libelle'))]->id_STATUTS);
    $statutID_ATTENTE = intval($statutsBDD[array_search('EN ATTENTE', array_column($statutsBDD, 'libelle'))]->id_STATUTS);
    $statutID_RETARD = intval($statutsBDD[array_search('EN RETARD', array_column($statutsBDD, 'libelle'))]->id_STATUTS);
    $statutID_TERMINER = intval($statutsBDD[array_search('TERMINÉ', array_column($statutsBDD, 'libelle'))]->id_STATUTS);
    $statutID_ABANDONNER = intval($statutsBDD[array_search('ABANDONNÉ', array_column($statutsBDD, 'libelle'))]->id_STATUTS);

    $countStatut_NON_DEMARRER = $statutsCount[$statutID_NON_DEMARRER];
    $countStatut_ABANDONNER = $statutsCount[$statutID_ABANDONNER];
    $countStatut_RETARD = $statutsCount[$statutID_RETARD];
    $countStatut_ATTENTE = $statutsCount[$statutID_ATTENTE];
    $countStatut_TERMINER = $statutsCount[$statutID_TERMINER];

    $lastIdStatutAction = new T_STATUT_ACTION_SERVICE_CAMPAGNE();
    $lastIdStatutAction = intval($lastIdStatutAction->getAllData("id_CAMPAGNES = {$id_CAMPAGNES} AND id_SERVICES = {$id_SERVICES} AND id_ACTIONS = {$id_ACTIONS} ORDER BY date DESC LIMIT 1")[0]->id_STATUTS);

    $newStatut = false;
    if(count($statuts) == $countStatut_NON_DEMARRER && $lastIdStatutAction != $statutID_NON_DEMARRER)
        $newStatut = $statutID_NON_DEMARRER;
    else if(count($statuts) == $countStatut_ABANDONNER && $lastIdStatutAction != $statutID_ABANDONNER)
        $newStatut = $statutID_ABANDONNER;
    else if($statutsCount[$statutID_RETARD] > 0 && $lastIdStatutAction != $statutID_RETARD)
        $newStatut = $statutID_RETARD;
    else if($statutsCount[$statutID_ATTENTE] > 0 && $lastIdStatutAction != $statutID_ATTENTE)
        $newStatut = $statutID_ATTENTE;
    else if(count($statuts) == ($statutsCount[$statutID_ABANDONNER] + $statutsCount[$statutID_TERMINER]) && $lastIdStatutAction != $statutID_TERMINER)
        $newStatut = $statutID_TERMINER;
    else if($lastIdStatutAction != $statutID_EN_COURS)
        $newStatut = $statutID_EN_COURS;

    if($newStatut){
        $statutAction = new T_STATUT_ACTION_SERVICE_CAMPAGNE();
        $statutAction->id_STATUTS = $newStatut;
        $statutAction->id_CAMPAGNES = $id_CAMPAGNES;
        $statutAction->id_SERVICES = $id_SERVICES;
        $statutAction->id_ACTIONS = $id_ACTIONS;
        $statutAction->date = $currentDate->format('Y-m-d H:i:s');
        $statutAction->user_id = intval(TApplication::getUserLogged()['user']->getId());
        $statutAction->insert();
    }

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors
    ));
    exit;

});