<?php

include_once(realpath(dirname(__FILE__).'/../../../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

$app->post(API_URL_PREFIX.'/actions_campagne/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = array();

    $services = new T_SERVICE();
    $services = $services->getAllData();

    $tab[] = 'id_ACTIONS';
    $tab[] = 'id_CAMPAGNES';
    $tab[] = 'action';

    foreach ($services as $service){
        $tab[] = 'statut'.$service->getId();
        $tab[] = 'color'.$service->getId();
    }

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult('digiprojects_');

    echo json_encode($result->read('V_ACTIONS_CAMPAGNES', $tab, $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->post(API_URL_PREFIX.'/actions_service/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'id_ACTIONS,id_SERVICES,id_CAMPAGNES,service,action,statut,statut_color';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult('digiprojects_');

    echo json_encode($result->read('V_ACTIONS_SERVICES', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});