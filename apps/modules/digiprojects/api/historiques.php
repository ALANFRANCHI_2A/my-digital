<?php

include_once(realpath(dirname(__FILE__).'/../../../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

$app->get(API_URL_PREFIX.'/historiques/{id_CAMPAGNES}/{id_ACTIONS}/{id_CIBLES}', function($id_CAMPAGNES, $id_ACTIONS, $id_CIBLES) use ($app, $twig){

    $html = "";

    $id_ACTIONS = intval($id_ACTIONS);
    $id_CIBLES = intval($id_CIBLES);
    $criteriaAction = ($id_ACTIONS > 0) ? "= {$id_ACTIONS}" : 'IS NULL';
    $criteriaCible = ($id_CIBLES > 0) ? "= {$id_CIBLES}" : 'IS NULL';

    $historiques = TDatabase::getDataAssoc('digiprojects_.V_HISTORIQUES', "id_CAMPAGNES = {$id_CAMPAGNES} AND id_ACTIONS {$criteriaAction} AND id_CIBLES {$criteriaCible}");

    $template = $twig->loadTemplate('digiprojets_block-historique.twig');
    foreach ($historiques as $key => $history){
        $params = array(
            'history' => $history,
            'position' => ($key%2) ? 'right' : 'left'
        );
        $html .= $template->render($params);
    }

    echo json_encode(array(
        'html' => $html,
    ));
    exit;

});