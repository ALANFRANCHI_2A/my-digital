<?php

include_once(realpath(dirname(__FILE__).'/../../../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

$app->get(API_URL_PREFIX.'/bdd/reset', function() use ($app){

    $sql = array();

    $sql[] =    "DELETE FROM `digiprojects_`.`TYPES_COMMUNICATIONS_CIBLES`";

    $sql[] =    "DELETE FROM `digiprojects_`.`REFS_EMAILING`";
    $sql[] =    "DELETE FROM `digiprojects_`.`REFS_EMESSAGE`";
    $sql[] =    "DELETE FROM `digiprojects_`.`REFS_NPC`";
    $sql[] =    "DELETE FROM `digiprojects_`.`REFS_OPC`";
    $sql[] =    "DELETE FROM `digiprojects_`.`REFS_SMS`";
    $sql[] =    "DELETE FROM `digiprojects_`.`REFS_TYPES_COMMUNICATIONS_CIBLES`";

    $sql[] =    "DELETE FROM `digiprojects_`.`HISTORIQUES`";
    $sql[] =    "ALTER TABLE `digiprojects_`.`HISTORIQUES` auto_increment = 1;";

    $sql[] =    "DELETE FROM `digiprojects_`.`ATTACHEMENTS`";
    $sql[] =    "ALTER TABLE `digiprojects_`.`ATTACHEMENTS` auto_increment = 1;";

    $sql[] =    "DELETE FROM `digiprojects_`.`COMMENTAIRES` WHERE id_COMMENTAIRES_PARENT IS NOT NULL";
    $sql[] =    "DELETE FROM `digiprojects_`.`COMMENTAIRES`";
    $sql[] =    "ALTER TABLE `digiprojects_`.`COMMENTAIRES` auto_increment = 1;";

    $sql[] =    "DELETE FROM `digiprojects_`.`ACTIONS_SERVICES_CAMPAGNES`";
    $sql[] =    "DELETE FROM `digiprojects_`.`STATUTS_ACTIONS_SERVICES_CAMPAGNES`";
    $sql[] =    "DELETE FROM `digiprojects_`.`STATUTS_TYPES_COMMUNICATIONS_CIBLES_CAMPAGNES`";

    $sql[] =    "DELETE FROM `digiprojects_`.`CIBLES`";
    $sql[] =    "ALTER TABLE `digiprojects_`.`CIBLES` auto_increment = 1;";

    $sql[] =    "DELETE FROM `digiprojects_`.`CAMPAGNES`";
    $sql[] =    "ALTER TABLE `digiprojects_`.`CAMPAGNES` auto_increment = 1;";

    foreach ($sql as $req)
        TDatabase::excuteSqlQuery($req, TDatabase::connect('digiprojects_'));

    echo 'BDD RESET - OK';
    exit;

});

$app->get(API_URL_PREFIX.'/vues/create', function() use ($app){

    $req = Request::createFromGlobals();

    $sql = array();
    $sql[] =    "DROP VIEW IF EXISTS V_ACTIONS_SERVICES;
                CREATE VIEW V_ACTIONS_SERVICES AS
                SELECT STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES,
                STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_SERVICES,
                STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_ACTIONS,
                SERVICES.libelle AS service,
                ACTIONS.libelle AS action,
                STATUTS.libelle AS statut,
                STATUTS.color AS statut_color,
                CONCAT(USERS.prenom,' ',USERS.nom) AS nom_complet
                FROM digiprojects_.STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_1, digiprojects_.SERVICES AS SERVICES, digiprojects_.ACTIONS AS ACTIONS, digiprojects_.STATUTS AS STATUTS, apps_gestion.UTILISATEURS AS USERS
                WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.date = (
                    SELECT MAX(STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.date)
                    FROM digiprojects_.STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_2
                    WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_CAMPAGNES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_SERVICES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_SERVICES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_ACTIONS = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_ACTIONS
                )
                AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_SERVICES = SERVICES.id_SERVICES
                AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_ACTIONS = ACTIONS.id_ACTIONS
                AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_STATUTS = STATUTS.id_STATUTS
                AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.user_id = USERS.id";

    $sql[] =    "DROP VIEW IF EXISTS V_CAMPAGNES;
                CREATE VIEW V_CAMPAGNES AS
                SELECT CAMPAGNES.*, CONCAT(apps_gestion.UTILISATEURS.prenom, ' ', apps_gestion.UTILISATEURS.nom) AS nom_complet, UNIVERS_BESOINS.libelle AS univers, PMA.libelle AS pma, (
                    SELECT COUNT(*)
                    FROM STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_1
                    WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.date = (
                    SELECT MAX(STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.date)
                        FROM STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_2
                        WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_CAMPAGNES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_SERVICES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_SERVICES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_ACTIONS = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_ACTIONS
                    )
                      AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES = CAMPAGNES.id_CAMPAGNES
                ) AS nb_actions, (
                    SELECT COUNT(*)
                           FROM STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_1
                           WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.date = (
                    SELECT MAX(STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.date)
                               FROM STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_2
                               WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_CAMPAGNES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_SERVICES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_SERVICES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_ACTIONS = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_ACTIONS
                           )
                             AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES = CAMPAGNES.id_CAMPAGNES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_STATUTS = 1
                       ) AS nb_non_demarrer, (
                    SELECT COUNT(*)
                           FROM STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_1
                           WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.date = (
                    SELECT MAX(STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.date)
                               FROM STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_2
                               WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_CAMPAGNES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_SERVICES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_SERVICES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_ACTIONS = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_ACTIONS
                           )
                             AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES = CAMPAGNES.id_CAMPAGNES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_STATUTS NOT IN (1,5,4,6)
                       ) AS nb_en_cours, (
                    SELECT COUNT(*)
                           FROM STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_1
                           WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.date = (
                    SELECT MAX(STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.date)
                               FROM STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_2
                               WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_CAMPAGNES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_SERVICES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_SERVICES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_ACTIONS = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_ACTIONS
                           )
                             AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES = CAMPAGNES.id_CAMPAGNES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_STATUTS = 5
                       ) AS nb_terminer, (
                    SELECT COUNT(*)
                           FROM STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_1
                           WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.date = (
                    SELECT MAX(STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.date)
                               FROM STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_2
                               WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_CAMPAGNES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_SERVICES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_SERVICES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_ACTIONS = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_ACTIONS
                           )
                             AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES = CAMPAGNES.id_CAMPAGNES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_STATUTS = 6
                       ) AS nb_abandonner, (
                    SELECT COUNT(*)
                           FROM STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_1
                           WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.date = (
                    SELECT MAX(STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.date)
                               FROM STATUTS_ACTIONS_SERVICES_CAMPAGNES AS STATUTS_ACTIONS_SERVICES_CAMPAGNES_2
                               WHERE STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_CAMPAGNES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_SERVICES = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_SERVICES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_2.id_ACTIONS = STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_ACTIONS
                           )
                             AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_CAMPAGNES = CAMPAGNES.id_CAMPAGNES
                    AND STATUTS_ACTIONS_SERVICES_CAMPAGNES_1.id_STATUTS = 4
                       ) AS nb_retard
                FROM CAMPAGNES
                         LEFT JOIN UNIVERS_BESOINS ON CAMPAGNES.id_UNIVERS_BESOINS = UNIVERS_BESOINS.id_UNIVERS_BESOINS
                         LEFT JOIN PMA ON CAMPAGNES.id_PMA = PMA.id_PMA
                         LEFT JOIN apps_gestion.UTILISATEURS ON CAMPAGNES.user_id = apps_gestion.UTILISATEURS.id
                ORDER BY CAMPAGNES.id_CAMPAGNES";

    $sql[] =    "DROP VIEW IF EXISTS V_COMMENTAIRES;
                CREATE VIEW V_COMMENTAIRES AS
                SELECT COMMENTAIRES.*, DATE_FORMAT(COMMENTAIRES.date_commentaire, \"%d/%m/%Y %H:%i\") AS date_fr, CONCAT(apps_gestion.UTILISATEURS.prenom,' ',apps_gestion.UTILISATEURS.nom) AS nom_complet
                FROM COMMENTAIRES
                    LEFT JOIN apps_gestion.UTILISATEURS ON COMMENTAIRES.user_id = apps_gestion.UTILISATEURS.id";

    $sql[] =    "DROP VIEW IF EXISTS V_HISTORIQUES;
                CREATE VIEW V_HISTORIQUES AS
                SELECT HISTORIQUES.*, DATE_FORMAT(HISTORIQUES.date_historique, \"%d/%m/%Y %H:%i\") AS date_fr, CONCAT(apps_gestion.UTILISATEURS.prenom,' ',apps_gestion.UTILISATEURS.nom) AS nom_complet
                FROM HISTORIQUES
                    LEFT JOIN apps_gestion.UTILISATEURS ON HISTORIQUES.user_id = apps_gestion.UTILISATEURS.id;";

    $sql[] =    "DROP VIEW IF EXISTS V_ATTACHEMENTS;
                CREATE VIEW V_ATTACHEMENTS AS
                SELECT ATTACHEMENTS.*, CONCAT(apps_gestion.UTILISATEURS.prenom,' ',apps_gestion.UTILISATEURS.nom) AS nom_complet
                FROM ATTACHEMENTS
                    LEFT JOIN apps_gestion.UTILISATEURS ON ATTACHEMENTS.user_id = apps_gestion.UTILISATEURS.id;";

    foreach ($sql as $req){
        TDatabase::excuteSqlQuery($req, TDatabase::connect('digiprojects_'));
    }

    $service = new T_SERVICE();
    $service->updateViewActionsServices();

    $type_communication = new T_TYPE_COMMUNICATION();
    $type_communication->updateViewCibles();

    echo 'VUES AJOUTES AVEC SUCCES';
    exit;

});