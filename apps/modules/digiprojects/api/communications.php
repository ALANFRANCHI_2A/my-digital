<?php

include_once(realpath(dirname(__FILE__).'/../../../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

$app->get(API_URL_PREFIX.'/communications/list', function() use ($app){

    $types_communications = new T_TYPE_COMMUNICATION();
    $types_communications = $types_communications->getAllData();

    echo json_encode(array(
        'types_communications' => $types_communications
    ));
    exit;

});

$app->get(API_URL_PREFIX.'/communications/{id_CAMPAGNES}/{id_ACTIONS}/{id_SERVICES}', function($id_CAMPAGNES, $id_ACTIONS, $id_SERVICES) use ($app, $twig){

    $html = "";

    $cilbes = new T_CIBLE();
    $cilbesID = array_column($cilbes->getAllData('id_CAMPAGNES = ' . $id_CAMPAGNES), 'id_CIBLES');
    $cilbesID = "'".implode("','", $cilbesID)."'";

    $action = new T_ACTION();
    $action->getFromDbById($id_ACTIONS);
    $type_communication = $action->getTypeCommunication();

    if(intval($type_communication->id_TYPES_COMMUNICATIONS) == 3){

        $refs_npc = TDatabase::getDataAssoc('REFS_NPC', "id_CIBLES IN ({$cilbesID}) AND id_TYPES_COMMUNICATIONS = {$type_communication->id_TYPES_COMMUNICATIONS}", 'DISTINCT code_projet, id_TYPES_COMMUNICATIONS', TDatabase::connect('digiprojects_'));

        foreach ($refs_npc as $ref_npc){

            if(!is_null($ref_npc['code_projet'])){
                $template = $twig->loadTemplate('digiprojets_block-ref-comm.twig');
                $params = array(
                    'ref' => $ref_npc
                );
                $html .= $template->render($params);
            }

            $refs_communication = new T_REF_NPC();
            $code_projet_CRITERIA = (is_null($ref_npc['code_projet'])) ? 'code_projet IS NULL' : "code_projet = '{$ref_npc['code_projet']}'";
            $refs_communication = $refs_communication->getAllData("id_CIBLES IN ({$cilbesID}) AND id_TYPES_COMMUNICATIONS = {$type_communication->id_TYPES_COMMUNICATIONS} AND {$code_projet_CRITERIA}");

            foreach ($refs_communication as $ref_communication){

                $statut = new T_STATUT_TYPE_COMMUNICATION_CIBLE_CAMPAGNE();
                $statut = $statut->getAllData("id_CAMPAGNES = {$id_CAMPAGNES} AND id_TYPES_COMMUNICATIONS = {$type_communication->id_TYPES_COMMUNICATIONS} AND id_CIBLES = {$ref_communication->id_CIBLES} AND id_SERVICES = {$id_SERVICES} ORDER BY date DESC LIMIT 1");
                $statut = (count($statut) === 1) ? $statut[0]->getStatut() : new T_STATUT();

                $template = $twig->loadTemplate('digiprojets_block-ref-comm.twig');
                $sub = (is_null($ref_npc['code_projet'])) ? '' : ' sub-media';
                $params = array(
                    'ref' => $ref_communication,
                    'statut' => $statut,
                    'sub' => $sub
                );
                $html .= $template->render($params);

            }

        }

    }else{

        $refs_communication = new T_REF_TYPE_COMMUNICATION_CIBLE();
        $refs_communication = $refs_communication->getAllData("id_CIBLES IN ({$cilbesID}) AND id_TYPES_COMMUNICATIONS = {$type_communication->id_TYPES_COMMUNICATIONS}");

        foreach ($refs_communication as $ref_communication){

            $statut = new T_STATUT_TYPE_COMMUNICATION_CIBLE_CAMPAGNE();
            $statut = $statut->getAllData("id_CAMPAGNES = {$id_CAMPAGNES} AND id_TYPES_COMMUNICATIONS = {$type_communication->id_TYPES_COMMUNICATIONS} AND id_CIBLES = {$ref_communication->id_CIBLES} AND id_SERVICES = {$id_SERVICES} ORDER BY date DESC LIMIT 1");
            $statut = (count($statut) === 1) ? $statut[0]->getStatut() : new T_STATUT();

            $template = $twig->loadTemplate('digiprojets_block-ref-comm.twig');
            $params = array(
                'ref' => $ref_communication,
                'statut' => $statut
            );
            $html .= $template->render($params);

        }

    }


    echo json_encode(array(
        'html' => $html
    ));
    exit;

});
