<?php

include_once(realpath(dirname(__FILE__).'/../../../model/datasourceresult.php'));

use Symfony\Component\HttpFoundation\Request;

$app->post(API_URL_PREFIX.'/config/types_communications/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'id_TYPES_COMMUNICATIONS,libelle';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult('digiprojects_');

    echo json_encode($result->read('TYPES_COMMUNICATIONS', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->post(API_URL_PREFIX.'/config/type_communication', function() use ($app){

    $req = Request::createFromGlobals();
    $errors = [];
    $result = false;

    $id_TYPES_COMMUNICATIONS = ($req->request->has('id_TYPES_COMMUNICATIONS')) ? intval($req->request->get('id_TYPES_COMMUNICATIONS')) : 0;
    $libelle = ($req->request->has('libelle')) ? $req->request->get('libelle') : null;

    if(is_null($libelle) || empty($libelle))
        $errors[] = 'Libelle requis';

    $types_communications = new T_TYPE_COMMUNICATION();
    $types_communications->getFromDbById($id_TYPES_COMMUNICATIONS);
    $types_communications->libelle = $libelle;

    if(count($errors) === 0)
        $result = $types_communications->save();

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors
    ));
    exit;

});

$app->post(API_URL_PREFIX.'/config/actions/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'id_ACTIONS,libelle';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult('digiprojects_');

    echo json_encode($result->read('ACTIONS', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->get(API_URL_PREFIX.'/config/action/{id_ACTIONS}', function($id_ACTIONS) use ($app){

    $html =  "<option>Aucune</option>";

    $action = new T_ACTION();
    $action->getFromDbById($id_ACTIONS);

    $types_communications = new T_TYPE_COMMUNICATION();
    $types_communications = $types_communications->getAllData();

    foreach ($types_communications as $type_communication)
        $html .= "<option value='{$type_communication->id_TYPES_COMMUNICATIONS}' ". (($type_communication->id_TYPES_COMMUNICATIONS == $action->id_TYPES_COMMUNICATIONS) ? 'selected' : '') .">{$type_communication->libelle}</option>";

    echo json_encode(array(
        'types_communications' => $html
    ));
    exit;

});
$app->post(API_URL_PREFIX.'/config/action', function() use ($app){

    $req = Request::createFromGlobals();
    $errors = [];
    $result = false;

    $id_ACTIONS = ($req->request->has('id_ACTIONS')) ? intval($req->request->get('id_ACTIONS')) : 0;
    $libelle = ($req->request->has('libelle')) ? $req->request->get('libelle') : null;
    $id_TYPES_COMMUNICATIONS = ($req->request->has('id_TYPES_COMMUNICATIONS')) ? intval($req->request->get('id_TYPES_COMMUNICATIONS')) : null;

    if(is_null($libelle) || empty($libelle))
        $errors[] = 'Libelle requis';

    if(is_null($id_TYPES_COMMUNICATIONS) || empty($id_TYPES_COMMUNICATIONS))
        $id_TYPES_COMMUNICATIONS = null;

    $action = new T_ACTION();
    $action->getFromDbById($id_ACTIONS);
    $action->libelle = $libelle;
    $action->id_TYPES_COMMUNICATIONS = $id_TYPES_COMMUNICATIONS;

    if(count($errors) === 0)
        $result = $action->save();

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors
    ));
    exit;

});

$app->post(API_URL_PREFIX.'/config/services/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'id_SERVICES,libelle';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult('digiprojects_');

    echo json_encode($result->read('SERVICES', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->get(API_URL_PREFIX.'/config/service/{id_SERVICES}', function($id_SERVICES) use ($app){

    $html_actions = $html_users =  "";

    $actionsService = new T_ACTION_SERVICE();
    $actionsService = $actionsService->getAllData("id_SERVICES = {$id_SERVICES}");
    $actionsService = array_column($actionsService, 'id_ACTIONS');

    $actions = new T_ACTION();
    $actions = $actions->getAllData();

    foreach ($actions as $action)
        $html_actions .= "<option value='{$action->id_ACTIONS}' ". ((in_array($action->id_ACTIONS, $actionsService)) ? 'selected' : '') .">{$action->libelle}</option>";

    $usersService = new T_USER_SERVICE();
    $usersService = $usersService->getAllData("id_SERVICES = {$id_SERVICES}");
    $usersService = array_column($usersService, 'id_USERS');

    $users = new T_UTILISATEUR();
    $users = $users->getAllData();

    foreach ($users as $user)
        $html_users .= "<option value='{$user->id}' ". ((in_array($user->id, $usersService)) ? 'selected' : '') .">{$user->prenom} {$user->nom}</option>";

    echo json_encode(array(
        'actions' => $html_actions,
        'users' => $html_users
    ));
    exit;

});
$app->post(API_URL_PREFIX.'/config/service', function() use ($app){

    $req = Request::createFromGlobals();
    $errors = [];
    $result = false;

    $id_SERVICES = ($req->request->has('id_SERVICES')) ? intval($req->request->get('id_SERVICES')) : 0;
    $libelle = ($req->request->has('libelle')) ? $req->request->get('libelle') : null;
    $ids_ACTIONS_SERVICES = ($req->request->has('actions_service')) ? $req->request->get('actions_service') : null;
    $ids_USERS_SERVICES = ($req->request->has('users_service')) ? $req->request->get('users_service') : null;

    if(is_null($libelle) || empty($libelle))
        $errors[] = 'Libelle requis';

    $service = new T_SERVICE();
    $service->getFromDbById($id_SERVICES);
    $service->libelle = $libelle;

    if(count($errors) === 0){
        $result = $service->save();
        TDatabase::excuteSqlQuery("DELETE FROM `ACTIONS_SERVICES` WHERE `ACTIONS_SERVICES`.`id_SERVICES` = {$id_SERVICES}", TDatabase::connect('digiprojects_'));
        foreach ($ids_ACTIONS_SERVICES as $id_ACTIONS_SERVICE)
            TDatabase::excuteSqlQuery("INSERT INTO `ACTIONS_SERVICES` (`id_SERVICES`, `id_ACTIONS`) VALUES ('{$id_SERVICES}', '{$id_ACTIONS_SERVICE}')", TDatabase::connect('digiprojects_'));
        TDatabase::excuteSqlQuery("DELETE FROM `USERS_SERVICES` WHERE `USERS_SERVICES`.`id_SERVICES` = {$id_SERVICES}", TDatabase::connect('digiprojects_'));
        foreach ($ids_USERS_SERVICES as $id_USERS_SERVICES)
            TDatabase::excuteSqlQuery("INSERT INTO `USERS_SERVICES` (`id_USERS`, `id_SERVICES`) VALUES ('{$id_USERS_SERVICES}', '{$id_SERVICES}')", TDatabase::connect('digiprojects_'));
    }

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors
    ));
    exit;

});

$app->post(API_URL_PREFIX.'/config/statuts/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'id_STATUTS,libelle,color';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult('digiprojects_');

    echo json_encode($result->read('STATUTS', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->get(API_URL_PREFIX.'/config/statut/{id_STATUTS}', function($id_STATUTS) use ($app){

    $html =  "";

    $statutsChilds = new T_STATUT_ENFANTS();
    $statutsChilds = $statutsChilds->getAllData("id_STATUTS = {$id_STATUTS}");
    $statutsChilds = array_column($statutsChilds, 'id_STATUTS_ENFANTS');

    $statuts = new T_STATUT();
    $statuts = $statuts->getAllData();

    foreach ($statuts as $statut)
        $html .= "<option value='{$statut->id_STATUTS}' ". ((in_array($statut->id_STATUTS, $statutsChilds)) ? 'selected' : '') .">{$statut->libelle}</option>";

    echo json_encode(array(
        'html' => $html
    ));
    exit;

});
$app->post(API_URL_PREFIX.'/config/statut', function() use ($app){

    $req = Request::createFromGlobals();
    $errors = [];
    $result = false;

    $id_STATUTS = ($req->request->has('id_STATUTS')) ? intval($req->request->get('id_STATUTS')) : 0;
    $libelle = ($req->request->has('libelle')) ? $req->request->get('libelle') : null;
    $color = ($req->request->has('color')) ? $req->request->get('color') : null;
    $ids_STATUTS_ENFANTS = ($req->request->has('statuts_chlids')) ? $req->request->get('statuts_chlids') : null;

    if(is_null($libelle) || empty($libelle))
        $errors[] = 'Libelle requis';

    $statut = new T_STATUT();
    $statut->getFromDbById($id_STATUTS);
    $statut->libelle = $libelle;
    $statut->color = $color;

    if(count($errors) === 0){
        $result = $statut->save();
        TDatabase::excuteSqlQuery("DELETE FROM `STATUTS_ENFANTS` WHERE `STATUTS_ENFANTS`.`id_STATUTS` = {$id_STATUTS}", TDatabase::connect('digiprojects_'));
        foreach ($ids_STATUTS_ENFANTS as $id_STATUTS_ENFANT)
            TDatabase::excuteSqlQuery("INSERT INTO `STATUTS_ENFANTS` (`id_STATUTS`, `id_STATUTS_ENFANTS`) VALUES ('{$id_STATUTS}', '{$id_STATUTS_ENFANT}')", TDatabase::connect('digiprojects_'));
    }

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors
    ));
    exit;

});

$app->post(API_URL_PREFIX.'/config/pma/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'id_PMA,libelle';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult('digiprojects_');

    echo json_encode($result->read('PMA', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->post(API_URL_PREFIX.'/config/pma', function() use ($app){

    $req = Request::createFromGlobals();
    $errors = [];
    $result = false;

    $id_PMA = ($req->request->has('id_PMA')) ? intval($req->request->get('id_PMA')) : 0;
    $libelle = ($req->request->has('libelle')) ? $req->request->get('libelle') : null;

    if(is_null($libelle) || empty($libelle))
        $errors[] = 'Libelle requis';

    $pma = new T_PMA();
    $pma->getFromDbById($id_PMA);
    $pma->libelle = $libelle;

    if(count($errors) === 0)
        $result = $pma->save();

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors
    ));
    exit;

});

$app->post(API_URL_PREFIX.'/config/poids_opc/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'id_POIDS_OPC,libelle';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult('digiprojects_');

    echo json_encode($result->read('POIDS_OPC', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->post(API_URL_PREFIX.'/config/poid_opc', function() use ($app){

    $req = Request::createFromGlobals();
    $errors = [];
    $result = false;

    $id_POIDS_OPC = ($req->request->has('id_POIDS_OPC')) ? intval($req->request->get('id_POIDS_OPC')) : 0;
    $libelle = ($req->request->has('libelle')) ? $req->request->get('libelle') : null;

    if(is_null($libelle) || empty($libelle))
        $errors[] = 'Libelle requis';

    $poid_opc = new T_POID_OPC();
    $poid_opc->getFromDbById($id_POIDS_OPC);
    $poid_opc->libelle = $libelle;

    if(count($errors) === 0)
        $result = $poid_opc->save();

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors
    ));
    exit;

});

$app->post(API_URL_PREFIX.'/config/sources_ciblages/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'id_SOURCES_CIBLAGES,libelle';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult('digiprojects_');

    echo json_encode($result->read('SOURCES_CIBLAGES', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->post(API_URL_PREFIX.'/config/source_ciblage', function() use ($app){

    $req = Request::createFromGlobals();
    $errors = [];
    $result = false;

    $id_SOURCES_CIBLAGES = ($req->request->has('id_SOURCES_CIBLAGES')) ? intval($req->request->get('id_SOURCES_CIBLAGES')) : 0;
    $libelle = ($req->request->has('libelle')) ? $req->request->get('libelle') : null;

    if(is_null($libelle) || empty($libelle))
        $errors[] = 'Libelle requis';

    $source_ciblage = new T_SOURCE_CIBLAGE();
    $source_ciblage->getFromDbById($id_SOURCES_CIBLAGES);
    $source_ciblage->libelle = $libelle;

    if(count($errors) === 0)
        $result = $source_ciblage->save();

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors
    ));
    exit;

});

$app->post(API_URL_PREFIX.'/config/univers_besoins/read', function() use ($app){

    $req = Request::createFromGlobals();
    $tab = 'id_UNIVERS_BESOINS,libelle';

    header('Content-Type: application/json');
    $request = json_decode($req->getContent(), false);
    $result = new DataSourceResult('digiprojects_');

    echo json_encode($result->read('UNIVERS_BESOINS', explode(',', $tab), $request, true), JSON_NUMERIC_CHECK);
    exit;

});
$app->post(API_URL_PREFIX.'/config/univer_besoin', function() use ($app){

    $req = Request::createFromGlobals();
    $errors = [];
    $result = false;

    $id_UNIVERS_BESOINS = ($req->request->has('id_UNIVERS_BESOINS')) ? intval($req->request->get('id_UNIVERS_BESOINS')) : 0;
    $libelle = ($req->request->has('libelle')) ? $req->request->get('libelle') : null;

    if(is_null($libelle) || empty($libelle))
        $errors[] = 'Libelle requis';

    $univer_besoin = new T_UNIVERS_BESOIN();
    $univer_besoin->getFromDbById($id_UNIVERS_BESOINS);
    $univer_besoin->libelle = $libelle;

    if(count($errors) === 0)
        $result = $univer_besoin->save();

    echo json_encode(array(
        'result' => $result,
        'errors' => $errors
    ));
    exit;

});