<?php

include_once(realpath(dirname(__FILE__).'/model/datamodel.php'));

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

$template = $twig->loadTemplate('digiprojets_email.twig');

$user = TApplication::getUserLogged()['user'];

$contenu = "<b>{$user->prenom} {$user->nom}</b> vient d'ajouter un nouveau commentaire : <br><br>";


$campagne = new T_CAMPAGNE();
$campagne->getFromDbById(1);
$action = new T_ACTION();
$action->getFromDbById(1);
$cible = new T_CIBLE();
$cible->getFromDbById(1);

$contenu .= "<b>CAMPAGNE :</b> " . $campagne->nom ."<br>";
$contenu .= "<b>ACTION :</b> " . $action->libelle ."<br>";
$contenu .= "<b>CIBLE :</b> CIBLE " . $cible->num ."<br>";
$contenu .= "<b>MESSAGE :</b> &laquo;&nbsp;<i>frefge</i>&nbsp;&raquo;";

$params = array(
    'user' => $user,
    'contenu' => $contenu
);

echo $template->render($params);