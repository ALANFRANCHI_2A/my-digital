<?php

use Symfony\Component\HttpFoundation\Request;

include_once(realpath(dirname(__FILE__).'/model/datamodel.php'));

$req = Request::createFromGlobals();

$guid = $req->query->has('guid') ? $req->query->get('guid') : '';
$user = new T_UTILISATEUR();
$user->getFromDbByProperty('guid', $guid);

if($user->getId() > 0){

    $template = $twig->loadTemplate('activation_compte.twig');
    $params = array(
        'user' => $user
    );
    echo $template->render($params);

}else{
    header('Location: index.php');
}