<?php

require(realpath(dirname(__FILE__).'/vendor/autoload.php'));

define('LOG_FILE_APP', realpath(dirname(__FILE__).'/../../data/log/app.log'));
define('LOG_FILE_SQL', realpath(dirname(__FILE__).'/../../data/log/sql.log'));

define('LOG_LEVEL', \Psr\Log\LogLevel::DEBUG);

function getLogger($logfile = LOG_FILE_APP){

    $logFormat = "[{date}] [{level}] {message}";
    $logger = new Katzgrau\KLogger\Logger("$logfile", LOG_LEVEL, array('logFormat' => $logFormat));

    return $logger;
}

?>